# -*- coding=utf-8 -*-

"""Script to help to publish a new release of the python package.

Usage
-----
```sh
python3.9 config/new_release.py
```

Warning
-------
This script is user-interactive.
"""


import sys
from datetime import date
from pathlib import Path

import tomli
from packaging.version import InvalidVersion, Version


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
PYPROJECT: Path = Path('pyproject.toml')
#
# Git
#
MAIN_BRANCH = 'master'
DEV_BRANCH = 'develop'


# ============================================================================ #
#                                   FUNCTIONS                                  #
# ============================================================================ #
def help_release():
    """Help to release a new package version."""
    current_version = get_current_version()
    new_version = get_new_version(current_version)
    new_release_branch(new_version)
    make_release_changes(new_version)
    commit_release_changes(new_version)
    merge_release_main(new_version)
    merge_release_develop(new_version)
    remove_release_branch(new_version)


def get_current_version() -> Version:
    """Get current version from toml.

    Returns
    -------
    Version
        Current version number
    """
    with open(PYPROJECT, mode='rb') as f_in:
        config = tomli.load(f_in)
    version_str: str = config['project']['version']
    try:
        current_version: Version = Version(version_str)
    except InvalidVersion:
        sys.exit(f'[ERROR] The current version `{version_str}` is not valid.')
    print(f'[INFO] The current version is: `{current_version}`')
    return current_version


def get_new_version(current_version: Version) -> Version:
    """Get the new version number.

    Parameters
    ----------
    current_version : Version
        Current version number to compare with

    Returns
    -------
    Version
        New correct version number
    """
    version_answer = input('[REQUEST] Please provide a new version number:\n')
    try:
        new_version = Version(version_answer)
    except InvalidVersion:
        sys.exit(
            f'[ERROR] The current version `{version_answer}` is not valid.')
    if current_version >= new_version:
        sys.exit('[ERROR] You must provide a new version number')
    if sum(l == '.' for l in str(new_version.base_version)) < 2:
        sys.exit(
            '[ERROR] You must provide a new version number'
            ' that respects the format `M.m.p`',
        )
    return new_version


def new_release_branch(new_version: Version):
    """New release branch message.

    Parameters
    ----------
    new_version : Version
        Version number
    """
    __wait_done_request(
        '[REQUEST] Create a release branch and enter `y`\n\n'
        f'git checkout -b release-{new_version} {DEV_BRANCH}\n',
    )


def make_release_changes(new_version: Version):
    """Make the release changes message.

    Parameters
    ----------
    new_version : Version
        Version number
    """
    __wait_done_request(
        f'[REQUEST] Changes for the release:\n'
        '* Change the version number in the `pyproject.toml` file'
        f' to {new_version}\n'
        '* Update the content of the `docs/src/changelog.md` file\n'
        '    * Release version and date'
        f' `[{new_version}] - {date.today()}`\n'
        '    * Please be carefull of the content\n'
        '* Then enter `y`\n',
    )


def commit_release_changes(new_version: Version):
    """Commit the release changes message.

    Parameters
    ----------
    new_version : Version
        Version number
    """
    __wait_done_request(
        '[REQUEST] Commit the changes and enter `y`\n\n'
        f'git commit -a -m "Bumped version number to {new_version}"\n',
    )


def merge_release_main(new_version: Version):
    """Merge the release and the main branches message.

    Parameters
    ----------
    new_version : Version
        Version number
    """
    __wait_done_request(
        f'[REQUEST] Merge to the {MAIN_BRANCH} branches and enter `y`\n\n'
        f'git checkout {MAIN_BRANCH}\n'
        f'git merge --no-ff release-{new_version} -m "Merge release"\n'
        f'git tag -a v{new_version} -m "Release v{new_version}"\n'
        f'git push origin {MAIN_BRANCH} --follow-tags\n',
    )


def merge_release_develop(new_version: Version):
    """Merge the release and the develop branches message.

    Parameters
    ----------
    new_version : Version
        Version number
    """
    __wait_done_request(
        f'[REQUEST] Merge to the {DEV_BRANCH} branches and enter `y`\n\n'
        f'git checkout {DEV_BRANCH}\n'
        f'git merge --no-ff release-{new_version} -m "Merge release"\n'
        f'git push origin {DEV_BRANCH} --follow-tags\n',
    )


def remove_release_branch(new_version: Version):
    """Remove the release branch message.

    Parameters
    ----------
    new_version : Version
        Version number
    """
    __wait_done_request(
        '[REQUEST] Remove the release branch and enter `y`\n\n'
        f'git branch -d release-{new_version}\n',
    )


def __wait_done_request(msg: str):
    done_answer = 'n'
    while done_answer != 'y':
        done_answer = input(msg)
    print()


# ============================================================================ #
#                                     MAIN                                     #
# ============================================================================ #
if __name__ == '__main__':
    help_release()
