#!/usr/bin/env bash

# USAGE:
#
#   * For developer:
#
#   ./config/install_venv39-dev.sh
#
# ============================================================================ #
#                                  PARAMETERS                                  #
# ============================================================================ #
#
# Here you can adapt what you want as requirements, virtual env. name etc.
#
# ---------------------------------------------------------------------------- #
#                         File And Directory Constants                         #
# ---------------------------------------------------------------------------- #
#
# Virtual environments names
#
venv_name=.venv_39-dev
#
# Config directory
#
config_dir=config

# ---------------------------------------------------------------------------- #
#                                 Requirements                                 #
# ---------------------------------------------------------------------------- #
requirements_dev=(
    "$config_dir/requirements-docs.txt"
    "$config_dir/requirements-linters.txt"
    "$config_dir/requirements-tests.txt"
)

# ---------------------------------------------------------------------------- #
#                              Log And Error Files                             #
# ---------------------------------------------------------------------------- #
stdout_f="$config_dir/std_out.txt"
stderr_f="$config_dir/std_err.txt"

# ============================================================================ #
#                          CREATE VIRTUAL ENVIRONMENT                          #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                   Constants                                  #
# ---------------------------------------------------------------------------- #
python_cmd=python3.9 # You should not change this

# ---------------------------------------------------------------------------- #
#                                   Functions                                  #
# ---------------------------------------------------------------------------- #
function die() {
    echo
    echo "ERROR: the last step failed"
    echo "    (i) You can find the standard and error outputs respectively in files:"
    echo "        $stdout_f"
    echo "        $stderr_f"
    clean_venv
    exit 1
}

function clean_logs() {
    rm $stdout_f 2>/dev/null
    rm $stderr_f 2>/dev/null
}

function clean_venv() {
    rm -r $venv_name 2>/dev/null
}

# ---------------------------------------------------------------------------- #
#                                     Clean                                    #
# ---------------------------------------------------------------------------- #
clean_venv
clean_logs

# ---------------------------------------------------------------------------- #
#                                Initialisation                                #
# ---------------------------------------------------------------------------- #
#
# Create logs files
#
touch $stdout_f
touch $stderr_f

echo "Install virtualenv initialiser"

$python_cmd -m pip install virtualenv >>$stdout_f 2>>$stderr_f || die

echo "Initialise the virtual environment $venv_name and activate it"

$python_cmd -m virtualenv $venv_name >>$stdout_f 2>>$stderr_f || die
source ./$venv_name/bin/activate >>$stdout_f 2>>$stderr_f || die
pip install "pip >=23.0, <23.1" --upgrade >>$stdout_f 2>>$stderr_f || die

# ---------------------------------------------------------------------------- #
#                                 Installation                                 #
# ---------------------------------------------------------------------------- #
if $dev; then
    echo "Install the requirements for developers"

    for requirement_file in ${requirements_dev[@]}; do
        echo "    Install $requirement_file"

        pip install -r $requirement_file >>$stdout_f 2>>$stderr_f || die
    done
    # necessary in editable mod for pytest
    pip install -e . >>$stdout_f 2>>$stderr_f || die
fi

echo "Deactivate the virtual environment"

deactivate >>$stdout_f 2>>$stderr_f || die

clean_logs
