## Summary

> Summarise the bug encountered concisely.

## Configuration

* **Package version:** `vM.m.p`
* **OS version:** `TODO`

## Example of buggy code

> How one can reproduce the issue - this is very important.

```python
# Python code
```

## What is the current bug behaviour?


## What is the expected correct behaviour?


## Relevant logs and/or screenshots

> Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as it's very hard to read otherwise.

## Possible fixes

> If you can, link to the line of code that might be responsible for the problem

/label ~bug