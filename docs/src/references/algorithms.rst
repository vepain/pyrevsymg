Algorithms
==========

.. toctree::
    :maxdepth: 2

    algo/cntdcomp
    algo/graph_func
    algo/trans_reduc