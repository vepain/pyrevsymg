Exceptions
==========

.. # DOC: just autosummary?

.. automodule:: revsymg.exceptions
    :autosummary:
    :members:
    :undoc-members:
    :inherited-members:

.. .. autoclass:: revsymg.exceptions.NoGraphAttribute()
..    :autosummary:
..    :members:
..    :undoc-members:
..    :inherited-members:

.. # FIXME: bug exception!