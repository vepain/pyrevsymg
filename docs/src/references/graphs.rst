Graphs
======

The main reverse symmetric graph is :class:`~revsymg.graphs.RevSymGraph`.

.. toctree::
    :maxdepth: 2

    graphs/revsym_graph

All the others are derived from user or algorithms.

- **From user:**

  .. toctree::
    :maxdepth: 1

    graphs/sub_graph

- **From algorithms:**

  .. toctree::
    :maxdepth: 1

    graphs/split_strands_graph