# Index library

```{module} revsymg.index_lib
```

To use one of the described component here, you have to import them from {mod}`revsymg.index_lib`.


## Types

|               Type |                                    Alias - value | Description                          |
| -----------------: | -----------------------------------------------: | ------------------------------------ |
|     {data}`IndexT` |                                     {class}`int` | Index type                           |
|        {data}`OrT` |                   {math}`\mathbb{B} = \{0 ; 1\}` | Orientations type                    |
|     {data}`IndOrT` |     {class}`tuple` ({data}`IndexT`, {data}`OrT`) | Oriented vertex type                 |
|    {data}`EIndOrT` |  {class}`tuple` ({data}`IndOrT`, {data}`IndOrT`) | Edges type                           |
|  {data}`IndOrIndT` |  {class}`tuple` ({data}`IndOrT`, {data}`IndexT`) | Adjacent vertex with edge index type |
| {data}`EIndOrIndT` | {class}`tuple` ({data}`EIndOrT`, {data}`IndexT`) | Edges with edge index type           |

<!-- DOC: EIndEIndT -->

```{eval-rst}
.. data:: IndexT

    Index type

.. data:: OrT

    Orientations type

    :value: binaries set :math:`\mathbb{B} = \{0 ; 1\}`

.. data:: IndOrT

    Oriented vertex type

    :value: :class:`tuple` (:data:`IndexT`, :data:`OrT`)

.. data:: EIndOrT

    Edges type.

    :value: :class:`tuple` (:data:`IndOrT`, :data:`IndOrT`)

.. data:: IndOrIndT

    Adjacent vertex with edge index type.

    :value: :class:`tuple` (:data:`IndOrT`, :data:`IndexT`)

.. data:: EIndOrIndT

    Edges with edge index type.

    :value: :class:`tuple` (:data:`EIndOrT`, :data:`IndexT`)
```

## Constants

* For {data}`OrT` type:
```{eval-rst}
.. autosummary::

    FORWARD_INT
    REVERSE_INT
    ORIENT_REV
    FWDOR_REVOR
```
* For {data}`IndOrT` type:
```{eval-rst}
.. autosummary::

    IND
    OR
```
* For {data}`EIndOrT` type:
```{eval-rst}
.. autosummary::

    PRED_IND
    SUCC_IND
```
* For {data}`IndOrIndT` type:
```{eval-rst}
.. autosummary::

    ADJ_INDOR
    ADJ_IND
```
* For {data}`EIndOrIndT` type:
```{eval-rst}
.. autosummary::

    E_IND
```
* Other:
```{eval-rst}
.. autosummary::

    MULT_IID
```
* All the constants
```{eval-rst}
.. autodata:: FORWARD_INT
.. autodata:: REVERSE_INT
.. autodata:: ORIENT_REV
.. autodata:: FWDOR_REVOR
.. autodata:: IND
.. autodata:: OR
.. autodata:: PRED_IND
.. autodata:: SUCC_IND
.. autodata:: ADJ_INDOR
.. autodata:: ADJ_IND
.. autodata:: E_IND
.. autodata:: MULT_IID
```


## Functions

* For {data}`IndOrT` type:
```{eval-rst}
.. autosummary::

    is_forward
    is_reverse
    rev_vertex
```
* For {data}`EIndOrT` type:
```{eval-rst}
.. autosummary::

    rev_edge
    is_canonical
    eindor_orientation
```
* All the functions:
```{eval-rst}
.. autofunction:: is_forward
.. autofunction:: is_reverse
.. autofunction:: rev_vertex
.. autofunction:: rev_edge
.. autofunction:: is_canonical
.. autofunction:: eindor_orientation
```
