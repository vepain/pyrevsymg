Connected Component Algorithms
==============================

# DOC: class and algo cnnted comp


.. automodule:: revsymg.algorithms.connected_components
    :autosummary:
    :members:
    :undoc-members:
    :inherited-members: