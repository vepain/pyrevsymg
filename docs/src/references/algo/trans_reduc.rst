Transitive Reduction
====================

# DOC: adapt doc

.. module:: revsymg.algorithms.transitive_reduction

Function
--------

.. autosummary::

    transitive_reduction

.. autofunction:: transitive_reduction

Data
----

.. autosummary::

    FUZZ
    OV_LEN_STR
    READ_LEN_STR

.. autodata:: FUZZ

.. autodata:: OV_LEN_STR

.. autodata:: READ_LEN_STR


# DOC: Error, docstring is not followed, see https://github.com/sphinx-doc/sphinx/issues/6495