Attributes Container
====================

.. autoclass:: revsymg.graphs.AttributesContainer()
   :autosummary:
   :members:
   :undoc-members:
   :inherited-members:
