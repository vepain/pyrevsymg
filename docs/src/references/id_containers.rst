Identifier Containers
=====================

An identifier container is an object which hashes identifiers.

*e.g.* it associates a read's identifier to an index.

So :math:`\forall r \in \mathcal{R}` we define:

    * :math:`r_{rid}` the read's identifier
    * :math:`r_{ind}` the read's index

You can write your own identifier container class using the abstract base class.

.. list-table::
    :header-rows: 1

    * - Class
      - Description
    * - :class:`~revsymg.id_containers.AbstractIDContainer`
      - Abstract base class for new identifier container
    * - :class:`~revsymg.id_containers.IndexIDContainer`
      - For 0-based indices identifiers
    * - :class:`~revsymg.id_containers.HashableIDContainer`
      - For hashable identifiers

.. # DOC: exception NoReadID

Type
----

.. data:: IdT

    :value: :class:`~typing.Hashable`

    Identifier type.

Indices Identifier Container
----------------------------

.. autoclass:: revsymg.id_containers.IndexIDContainer()
    :autosummary:
    :members:
    :undoc-members:
    :inherited-members:

Hashable Identifier Container
-----------------------------

.. autoclass:: revsymg.id_containers.HashableIDContainer()
   :autosummary:
   :members:
   :undoc-members:
   :inherited-members:

Identifier Container Abstract Base Class
----------------------------------------

.. autoclass:: revsymg.id_containers.AbstractIDContainer()
   :autosummary:
   :members:
   :undoc-members:
   :inherited-members: