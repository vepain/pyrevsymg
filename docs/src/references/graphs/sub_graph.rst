Sub-Graphs
==========

.. Graph
.. -----

.. .. autoclass:: revsymg.graphs.view.SubRevSymGraph()
..    :autosummary:
..    :members:
..    :undoc-members:
..    :inherited-members:

.. Vertices container
.. ---------------

.. .. autoclass:: revsymg.graphs.view.SubVertices()
..    :autosummary:
..    :members:
..    :undoc-members:
..    :inherited-members:

.. Edges container
.. ---------------

.. .. autoclass:: revsymg.graphs.view.SubEdges()
..    :autosummary:
..    :members:
..    :undoc-members:
..    :inherited-members: