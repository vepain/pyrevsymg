# -*- coding=utf-8 -*-
"""Configuration file for the Sphinx documentation builder.

This file only contains a selection of the most common options. For a full
list see the documentation:
https://www.sphinx-doc.org/en/master/usage/configuration.html
"""

# pylint: disable=invalid-name

from __future__ import annotations

from datetime import date
from json import load

import tomli


# ============================================================================ #
#                              PROJECT INFORMATION                             #
# ============================================================================ #
with open('../../pyproject.toml', mode='rb') as fp:
    __config = tomli.load(fp)

project = __config['project']['name']
author = ', '.join(d['name'] for d in __config['project']['authors'])
copyright = f'2021-{date.today().year}, {author}'  # noqa
# The full version, including alpha/beta/rc tags
release = __config['project']['version']

# ============================================================================ #
#                           EXTENSIONS AND TEMPLATES                           #
# ============================================================================ #
# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.napoleon',
    'sphinx.ext.coverage',
    'sphinx.ext.intersphinx',
    'sphinx.ext.autosummary',
    'autodocsumm',
    'myst_parser',
]

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['*test*']

# ---------------------------------------------------------------------------- #
#                        Project And Extensions Options                        #
# ---------------------------------------------------------------------------- #
add_module_names = False

intersphinx_mapping = {
    'python': ('https://docs.python.org/3.9', None),
}

# ---------------------------------------------------------------------------- #
#                                     MySt                                     #
# ---------------------------------------------------------------------------- #
myst_heading_anchors = 5

myst_enable_extensions = [
    'strikethrough',
    'dollarmath',
    'tasklist',
]

# ---------------------------------------------------------------------------- #
#                                    Autodoc                                   #
# ---------------------------------------------------------------------------- #
autodoc_default_options = {
    'special-members': '__iter__, __contains__, __len__',
}
autodoc_member_order = 'groupwise'
autodoc_typehints = 'none'
autodoc_typehints_description_target = 'documented'
autodoc_preserve_defaults = True

# ---------------------------------------------------------------------------- #
#                                 Manual Types                                 #
# ---------------------------------------------------------------------------- #
# Hyperlinks must be explicitly given because of cross-pages references.
# Tips:
#   - in docstring, references must give full module path, except for type
#   - add module path for types in json docs/src/references/aliases.json
# DOC: splitstrands & merged strands ovg aliases
with open('references/aliases.json', 'r', encoding='utf-8') as json:
    aliases = load(json)

napoleon_type_aliases = {
    name: f':class:`~{mod_path}.{name}`'
    for name, mod_path in aliases['class'].items()
}
napoleon_type_aliases.update(
    {
        name: f':data:`~{mod_path}.{name}`'
        for name, mod_path in aliases['data'].items()
    },
)

napoleon_use_admonition_for_notes = True
napoleon_preprocess_types = True
napoleon_use_ivar = True

# ---------------------------------------------------------------------------- #
modindex_common_prefix = [
    'revsymg.',
    'revsymg.algorithms.',
    'revsymg.algorithms.inputs_outputs.',
    'revsymg.lib.',
    'revsymg.graphs.',
    'revsymg.utils.',
    'revsymg.exceptions.',
]

# ============================================================================ #
#                            OPTIONS FOR HTML OUTPUT                           #
# ============================================================================ #
# The theme to use for HTML and HTML Help pages.
html_theme = 'furo'
html_logo = '../img/revsymg_logo_transp.png'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
