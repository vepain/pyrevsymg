# Install

## Distant

```sh
pip install git+https://gitlab.com/vepain/pyrevsymg@TAG
```

or (to be sure which python version you use if you are not in an activated virtual environment)
```sh
python3.X -m pip install git+https://gitlab.com/vepain/pyrevsymg@TAG
```
where `TAG` is the tag version (release number `M.m.p`).

## Local

For the latest commit:
```sh
git clone https://gitlab.com/vepain/pyrevsymg.git
cd pyrevsymg
pip install .
```

So that when you want to update the package, you just to type in the git directory:
```sh
git pull
pip install . --upgrade
```

## Aware

for `bitarray>=2.4.0, <2.5.0`, install `python3.9-dev`

On Ubuntu:
```sh
sudo apt install python3.9-dev
```