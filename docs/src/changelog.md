# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

> '**M**', '**m**' and '**p**' are respectively corresponding to major, minor and patch

## [0.4.1] - 2023-02-16

### Removed

* **p:** no longer need to depend on NetworkX


## [0.4.0] - 2023-02-16

### Removed

* **m:** module `utils` with `utils.viw_filters` were removed because they were considered useless

### Changed

* **m:** module `hash_ids` becomes `id_container`
* **m:** names of identifier container have been fixed for an English error
* **m:** use `from revsymg.index_lib import ...` and `from revsymg.str_lib import ...` instead of `from revsymg.lib[.(index_lib|str_lib)] import ...`
* **p:** refactoring docs files

### Added

* **p:** todo in doc


## [0.3.4] - 2023-02-12

* **p:** add mypy badge
* **p:** add mypy job in pipeline
* **p:** change job name for linters in pipeline
* **p:** move `GraphsT` and `GraphsCnntedCompT` types to the `cnnted_comp_tools.py` module


## [0.3.3] - 2023-02-10

### Fixed

* **p:** install the package for readthedocs to have references


## [0.3.2] - 2023-02-02

### Changes

* **p:** contributing doc
* **p:** fix missing changelog


## [0.3.1] - 2023-02-02

### Added

* **p:** use readthedocs for the documentation


## [0.3.0] - 2023-02-01

### Added

* **m:** new `Edges` method `Edges.biggest_edge_index`


## [0.2.0] - 2022-12-02

### Added

* Transitive reduction algorithm
* use an integer identifier for edges

### Changed

* don't use object for connected component identifiers to simulate pointers, but use a list of identifiers couples instead
* by default, removing a vertex implies shifting the vertex indices if the index of the removed vertex is not the biggest

### Removed

* path utilities module
* SimpleCnntedComp and DoubleCnntedComp