# What is next


## Graphs

* [ ] IDEA: to GFA function

### Graph views

* [ ] IDEA: graphs views
  * [ ] See `graphtools` python package
  * [ ] See `backup` folder


## Vertices

### Virtual suppress

* [ ] IDEA: virtual suppress vertices

Vertex is not suppressed in memory:
* index will not change

### Real suppress

* [x] Real suppress vertices

Vertex is suppressed in memory:
* Vertex index will change
* Dichotomy on number of removing pull update times $|E|$
* Delete attributes -> $O(|V|)$ if list
* [ ] DOC: explain complexity, verify with preprint
* [ ] TODO: verify if in code is same as in the preprint


## Edges

### Add edges

* [x] Add several edges $(u, v)$ same orientations?
  * Then search for `e_index` method should yield on them

### Edge indices

* [ ] TODO: forward and reverse edge should not have the same index
  * [ ] Do it as described in the preprint

### Virtual suppress

* [x] Virtual suppress edges

Edge is not suppressed in memory:
* Edge indices will not change
* [ ] DOC: explain complexity
* [ ] TODO: verify if in code is same as in the preprint


### Real suppress

* [ ] IDEA: Real suppress edges

* Edge is suppressed in memory:
  * Edge indices will change
  * Dichotomy on number of removing pull update times $|E|$
  * delete attributes -> $O(|E|)$ if list
* [ ] XXX: explain complexity, not sure about this result


### SplitStrandsEdges

* [ ] REFACTOR: orientations method (from _vertices attribute before) should be a shared attribute with vertices and edges
* [ ] IDEA: this class is for object representing one connected component, should I verify when delete/add the connectivity? -> fully dynamic connectivity
  * [ ] XXX: for now, we can have an attribute to say: certain or not to be in only one cc


## Packaging and Gitlab

### CI/CD

* [ ] TODO: script bump_release.sh (see https://nvie.com/posts/a-successful-git-branching-model/) that modify version in `pyptoject.toml`
* [ ] TODO: don't accept push to master if the version in pyproject.toml is the same as previously
  * [ ] TODO: scripts release push
* [x] auto-release gitlab