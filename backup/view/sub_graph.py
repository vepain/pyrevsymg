# -*- coding=utf-8 -*-

"""Sub overlaps graph view class module."""

from __future__ import annotations

from collections.abc import Iterable
from typing import Any

from revsymg.exceptions import NoGraphAttribute
from revsymg.graphs.attributes import AttributesContainer
from revsymg.graphs.view.abstract_subgraph import _SubOvGAbstract
from revsymg.graphs.view.sub_edges import SubEdges
from revsymg.graphs.view.sub_nodes import SubNodes
from revsymg.index_lib import ENODES, EIndOrIndT, IndOrT


# ============================================================================ #
#                              SUB OVERLAPS GRAPH                              #
# ============================================================================ #
class SubRevSymGraph(_SubOvGAbstract):
    """The SubRevSymGraph class."""

    def __init__(self, _subnodes: SubNodes, _subedges: SubEdges,
                 _nodesattrs: AttributesContainer,
                 _edgesattrs: AttributesContainer,
                 _attributes: dict[str, Any]):
        """The initializer."""
        # Nodes containers ---
        self._subnodes: SubNodes = _subnodes
        self._nodeattrs: AttributesContainer = _nodesattrs
        # Edges containers ---
        self._subedges: SubEdges = _subedges
        self._edgesattrs: AttributesContainer = _edgesattrs
        self._attributes: dict[str, Any] = _attributes.copy()

    # -*- Graph methods -*-

    def attr(self, attrname: str) -> Any:
        """Return the value associated to attribute `attrname`.

        Parameters
        ----------
        attrname : str
            Attribute name

        Returns
        -------
        Any
            Attribute value corresponding to the attribute name

        Raises
        ------
        NoGraphAttribute
            When there is no attribute key
        """
        try:
            return self._attributes[attrname]
        except KeyError as exc:
            raise NoGraphAttribute(attrname) from exc

    def attrs(self) -> dict[str, Any]:
        """Return the dictionnary of all attributes.

        Returns
        -------
        dict
            Dictionnary of attribute name as key and their value
        """
        return self._attributes

    # -*- Subgraph methods -*-

    def subgraph_from_nodes(self, nodes: Iterable[IndOrT]) -> SubRevSymGraph:
        """Return a sub overlaps graph from nodes.

        Parameters
        ----------
        nodes : iterable of IndOrT
            An iterable of nodes

        Returns
        -------
        SubRevSymGraph
            The overlaps graph masked by nodes
        """
        sub_nodes = SubNodes(self._subnodes.nodes())
        sub_edges = SubEdges(self._subnodes.nodes(), self._subedges.edges())
        for v in nodes:
            sub_nodes.add_to_mask(v)
        for v in sub_nodes:
            for w, e_ind in self.edges().succs(v):
                if w in sub_nodes:
                    sub_edges.add_to_mask(((v, w), e_ind))
        sub_nodes.combine(self._subnodes)
        sub_edges.combine(self._subedges)
        return SubRevSymGraph(
            sub_nodes, sub_edges,
            self._nodeattrs, self._edgesattrs, self._attributes,
        )

    def subgraph_from_edges(self,
                            edges: Iterable[EIndOrIndT]) -> SubRevSymGraph:
        """Return a sub overlaps graph from edges.

        Parameters
        ----------
        edges : iterable of EIndOrIndT
            An iterable of edges

        Returns
        -------
        SubRevSymGraph
            The overlaps graph masked by edges
        """
        sub_nodes = SubNodes(self._subnodes.nodes())
        sub_edges = SubEdges(self._subnodes.nodes(), self._subedges.edges())
        for edge in edges:
            sub_nodes.add_to_mask(edge[ENODES][0])
            sub_nodes.add_to_mask(edge[ENODES][1])
            sub_edges.add_to_mask(edge)
        sub_nodes.combine(self._subnodes)
        sub_edges.combine(self._subedges)
        return SubRevSymGraph(
            sub_nodes, sub_edges,
            self._nodeattrs, self._edgesattrs, self._attributes,
        )

    # -*- Getter -*-

    def nodes(self) -> SubNodes:
        """Return the nodes container.

        Returns
        -------
        SubNodes
            Sub nodes container
        """
        return self._subnodes

    def edges(self) -> SubEdges:
        """Return the edge container.

        Returns
        -------
        SubEdges
            Sub edges container
        """
        return self._subedges
