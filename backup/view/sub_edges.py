# -*- coding=utf-8 -*-

"""Sub edges container view class module."""

from __future__ import annotations

from collections.abc import Generator

from bitarray import bitarray

from revsymg.exceptions import NoEdgeIndex
from revsymg.graphs.revsym_edges import Edges
from revsymg.graphs.revsym_nodes import Nodes
from revsymg.graphs.view.abstract_subedges import _SubEdgesAbstract
from revsymg.index_lib import (
    E_IND,
    ENODES,
    FORWARD_INT,
    REVERSE_INT,
    EIndOrIndT,
    EIndOrT,
    IndOrIndT,
    IndOrT,
    eindor_orientation,
)


# ============================================================================ #
#                                     TYPE                                     #
# ============================================================================ #
EdgesMasksT = tuple[bitarray, bitarray]


# ============================================================================ #
#                              SUB EDGES CONTAINER                             #
# ============================================================================ #
class SubEdges(_SubEdgesAbstract):
    """The SubEdges container class.

    You must not call this class.
    Use it only for typing or get an already existing object.
    """

    def __init__(self, _nodes: Nodes, _edges: Edges):
        # XXX: use NodesAbstract instead of ReadsId
        self._nodes: Nodes = _nodes
        self._edges: Edges = _edges
        self._mask: EdgesMasksT = (
            bitarray('0') * len(self._edges),
            bitarray('0') * len(self._edges),
        )

    # -*- Masks -*-

    def add_to_mask(self, edge: EIndOrIndT):
        """Add `edge` to mask.

        Parameters
        ----------
        edge: EIndOrIndT
            Oriented edge with edge index
        """
        e_or = eindor_orientation(edge[ENODES])
        self._mask[e_or][edge[E_IND]] = True

    def combine(self, subedges: SubEdges):
        """Combine the self masks with `edges` ones.

        Parameters
        ----------
        subedges: SubEdges
            Sub edges container
        """
        forward_mask = self._mask[FORWARD_INT]
        forward_mask &= subedges.forward_mask()
        reverse_mask = self._mask[REVERSE_INT]
        reverse_mask &= subedges.reverse_mask()

    # -*- Edges -*-

    def neighbours(self, node: IndOrT) -> Generator[IndOrIndT, None, None]:
        """Return the generator of `node`'s adjacencies vertices.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Yields
        ------
        IndOrIndT
            Adjacent oriented nodes with edge index
        """
        yield from self.preds(node)
        yield from self.succs(node)

    def preds(self, node: IndOrT) -> Generator[IndOrIndT, None, None]:
        """Return the generator of predecessors of vertex `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Yields
        ------
        IndOrIndT
            Predecessor oriented node with edge index
        """
        for pred, e_ind in self._edges.preds(node):
            e_or = eindor_orientation((pred, node))
            if self._mask[e_or][e_ind]:
                yield pred, e_ind

    def succs(self, node: IndOrT) -> Generator[IndOrIndT, None, None]:
        """Return the generator of successors of vertex `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Yields
        ------
        IndOrIndT
            Successor oriented node with edge index
        """
        for succ, e_ind in self._edges.succs(node):
            e_or = eindor_orientation((node, succ))
            if self._mask[e_or][e_ind]:
                yield succ, e_ind

    # -*- Properties -*-

    def in_degree(self, node: IndOrT) -> int:
        """Return the in -degree of `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Returns
        -------
        int
            In-degree
        """
        in_degree = 0
        for pred, e_ind in self.preds(node):
            e_or = eindor_orientation((pred, node))
            if self._mask[e_or][e_ind]:
                in_degree += 1
        return in_degree

    def out_degree(self, node: IndOrT) -> int:
        """Return the out - degree of `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Returns
        -------
        int
            Out-degree
        """
        out_degree = 0
        for succ, e_ind in self.succs(node):
            e_or = eindor_orientation((node, succ))
            if self._mask[e_or][e_ind]:
                out_degree += 1
        return out_degree

    def degree(self, node: IndOrT) -> int:
        """Return the degree of `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Returns
        -------
        int
            Degree
        """
        degree = 0
        for pred, e_ind in self.preds(node):
            e_or = eindor_orientation((pred, node))
            if self._mask[e_or][e_ind]:
                degree += 1
        for succ, e_ind in self.succs(node):
            e_or = eindor_orientation((node, succ))
            if self._mask[e_or][e_ind]:
                degree += 1
        return degree

    # -*- Getter -*-

    def card_e(self) -> int:
        r"""Return the number of edges.

        Returns
        -------
        int
            Number of oriented edges

        Notes
        -----
        An overlap :math:`ov \in \mathcal{O}` corresponds to two recorded edges
        :math:`e \in E` and :math:`\bar{e} \in E` in edges container. But in
        sub edges container, the oriented edges are not always both together
        """
        # DOC: change note
        return self._mask[FORWARD_INT].count() + self._mask[REVERSE_INT].count()

    def edges(self) -> Edges:
        """Return the original edges.

        Returns
        -------
        Edges
            The original edges container
        """
        return self._edges

    def forward_mask(self) -> bitarray:
        """Return the forward mask.

        Returns
        -------
        bitarray
            Forward mask
        """
        return self._mask[FORWARD_INT]

    def reverse_mask(self) -> bitarray:
        """Return the reverse mask.

        Returns
        -------
        bitarray
            Reverse mask
        """
        return self._mask[REVERSE_INT]

    # -*- Overloaded built-in -*-

    def __iter__(self) -> Generator[EIndOrIndT, None, None]:
        """Iterate on oriented edges.

        Yields
        ------
        EIndOrIndT
            Oriented edges with their edge index
        """
        for (edge, e_ind) in self._edges:
            e_or = eindor_orientation(edge)
            if self._mask[e_or][e_ind]:
                yield edge, e_ind

    def __contains__(self, edge: EIndOrT) -> bool:
        """Return True if the oriented edge is in edges container.

        Parameters
        ----------
        edge: EIndOrT
            Oriented edge

        Returns
        -------
        bool
            Oriented edges in edges container
        """
        try:
            e_ind = self.eindor_to_eind(edge)
        except NoEdgeIndex:
            return False
        e_or = eindor_orientation(edge)
        return bool(self._mask[e_or][e_ind])

    def __len__(self) -> int:
        """Return the number of oriented edges.

        Returns
        -------
        int
            Number of oriented edges
        """
        return self._mask[FORWARD_INT].count() + self._mask[REVERSE_INT].count()
