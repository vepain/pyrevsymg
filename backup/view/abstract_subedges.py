# -*- coding=utf-8 -*-

"""Abstract base class for sub edges container module."""

from __future__ import annotations

from abc import abstractmethod
from collections.abc import Generator
from typing import Any

from revsymg.graphs.abstract_edges import _EdgesAbstract
from revsymg.graphs.attributes import AttributesContainer
from revsymg.index_lib import EIndOrIndT, EIndOrT, IndexT, IndOrIndT, IndOrT


# ============================================================================ #
#                         ABSTRACT SUB EDGES CONTAINER                         #
# ============================================================================ #
class _SubEdgesAbstract():
    """Abstract sub edges container class."""

    _edges: _EdgesAbstract

    # -*- Masks -*-

    @abstractmethod
    def add_to_mask(self, edge: EIndOrIndT):
        """Add `edge` to mask.

        Parameters
        ----------
        edge: EIndOrIndT
            Oriented edge with edge index
        """
        raise NotImplementedError

    # -*- Edges -*-

    @abstractmethod
    def neighbours(self, node: IndOrT) -> Generator[IndOrIndT, None, None]:
        """Return the generator of `node`'s adjacencies vertices.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Yields
        ------
        IndOrIndT
            Adjacent oriented nodes with edge index
        """
        raise NotImplementedError

    @abstractmethod
    def preds(self, node: IndOrT) -> Generator[IndOrIndT, None, None]:
        """Return the generator of predecessors of vertex `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Yields
        ------
        IndOrIndT
            Predecessor oriented node with edge index
        """
        raise NotImplementedError

    @abstractmethod
    def succs(self, node: IndOrT) -> Generator[IndOrIndT, None, None]:
        """Return the generator of successors of vertex `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Yields
        ------
        IndOrIndT
            Successor oriented node with edge index
        """
        raise NotImplementedError

    # -*- Attributes -*-

    def attr(self, edge: EIndOrIndT, attrname: str) -> Any:
        """Return attribute value of edge from its name.

        Parameters
        ----------
        edge : EIndOrIndT
            Oriented edge with its index
        attrname : str
            Attribute name

        Returns
        -------
        Any
            Attribute value

        Raises
        ------
        NoEdgeAttribute
            If there is no attribute name like this for the edge
        """
        return self._edges.attr(edge, attrname)

    def attrs(self, edge: EIndOrIndT) -> dict[str, Any]:
        """Return the dictionnary of all `edge`'s attributes.

        Parameters
        ----------
        edge: EIndOrIndT
            Oriented edge with edge index

        Returns
        -------
        dict
            Dictionnary, attribute name maps attribute value
        """
        # OPTIMIZE: use yield instead of dict
        return self._edges.attrs(edge)

    # -*- Properties -*-

    @abstractmethod
    def in_degree(self, node: IndOrT) -> int:
        """Return the in -degree of `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Returns
        -------
        int
            In-degree
        """
        raise NotImplementedError

    @abstractmethod
    def out_degree(self, node: IndOrT) -> int:
        """Return the out - degree of `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Returns
        -------
        int
            Out-degree
        """
        raise NotImplementedError

    @abstractmethod
    def degree(self, node: IndOrT) -> int:
        """Return the degree of `node`.

        Parameters
        ----------
        node: IndOrT
            Oriented node

        Returns
        -------
        int
            Degree
        """
        raise NotImplementedError

    # -*- Getter -*-

    @abstractmethod
    def card_e(self) -> int:
        r"""Return the number of edges.

        Returns
        -------
        int
            Number of oriented edges

        Notes
        -----
        An overlap :math:`ov \in \mathcal{O}` corresponds to two recorded edges
        :math:`e \in E` and :math:`\bar{e} \in E` in edges container. But in
        sub edges container, the oriented edges are not always both together
        """
        # DOC: change note
        raise NotImplementedError

    @abstractmethod
    def edges(self) -> _EdgesAbstract:
        """Return the original edges.

        Returns
        -------
        _EdgesAbstract
            The original edges container
        """
        return self._edges

    def attributes(self) -> AttributesContainer:
        """Return attributes object attribute.

        Returns
        -------
        AttributesContainer
            Attributes container
        """
        return self._edges.attributes()

    def eindor_to_eind(self, edge: EIndOrT) -> IndexT:
        """Return edge index from oriented edge.

        Parameters
        ----------
        edge : EIndOrT
            Oriented edge

        Returns
        -------
        IndexT
            Edge index

        Raises
        ------
        NoEdgeIndex
            If there is no oriented edge in edges container
        """
        return self._edges.eindor_to_eind(edge)

    # -*- Overloaded built-in -*-

    @abstractmethod
    def __iter__(self) -> Generator[EIndOrIndT, None, None]:
        """Iterate on oriented edges.

        Yields
        ------
        EIndOrIndT
            Oriented edges with their edge index
        """
        raise NotImplementedError

    @abstractmethod
    def __contains__(self, edge: EIndOrT) -> bool:
        """Return True if the oriented edge is in edges container.

        Parameters
        ----------
        edge: EIndOrT
            Oriented edge

        Returns
        -------
        bool
            Oriented edges in edges container
        """
        raise NotImplementedError

    @abstractmethod
    def __len__(self) -> int:
        """Return the number of oriented edges.

        Returns
        -------
        int
            Number of oriented edges
        """
        raise NotImplementedError
