# -*- coding=utf-8 -*-

"""Abstract base class for sub nodes container module."""

from __future__ import annotations

from abc import abstractmethod
from collections.abc import Generator
from typing import Any

from revsymg.graphs.abstract_nodes import _NodesAbstract
from revsymg.graphs.attributes import AttributesContainer
from revsymg.index_lib import IndOrT


# ============================================================================ #
#                         ABSTRACT SUB NODES CONTAINER                         #
# ============================================================================ #
class _SubNodesAbstract():
    """The SubNodes concrete class."""

    _nodes: _NodesAbstract

    # -*- Index -*-

    @abstractmethod
    def card_index(self) -> int:
        """Return the number of nodes' indices.

        Returns
        -------
        int
            Number of nodes' indices
        """
        raise NotImplementedError

    @abstractmethod
    def card_indors(self) -> int:
        """Return the number of oriented nodes.

        Returns
        -------
        int
            The number of oriented nodes
        """
        raise NotImplementedError

    # -*- Attributes -*-

    def attr(self, node: IndOrT, attrname: str) -> Any:
        """Return the value associated to `node` attribute `attrname`.

        Parameters
        ----------
        node : IndOrT
            Oriented node
        attrname : str
            Attribute name

        Returns
        -------
        Any
            Attribute value corresponding to the attribute name

        Raises
        ------
        NoVertexAttribute
            If node has no attribute name like that
        """
        return self._nodes.attr(node, attrname)

    def attrs(self, node: IndOrT) -> dict[str, Any]:
        """Return the dictionnary of all `node`'s attributes.

        Parameters
        ----------
        node : IndOrT
            Oriented node

        Returns
        -------
        dict
            Dictionnary of attribute name as key and their value
        """
        # OPTIMIZE: generator instead of dict
        return self._nodes.attrs(node)

    # -*- Getter -*-

    @abstractmethod
    def card_v(self) -> int:
        """Return the number of oriented nodes.

        Returns
        -------
        int
            Number of oriented nodes
        """
        raise NotImplementedError

    @abstractmethod
    def nodes(self) -> _NodesAbstract:
        """Return original nodes container.

        Returns
        -------
        NodesAbstract
            Nodes container
        """
        raise NotImplementedError

    def attributes(self) -> AttributesContainer:
        """Return attributes container.

        Returns
        -------
        AttributesContainer
            Attribute container
        """
        return self._nodes.attributes()

    # -*- Setter -*-

    @abstractmethod
    def add_to_mask(self, node: IndOrT):
        """Add oriented node to mask.

        Parameters
        ----------
        node: IndOrT
            Oriented node
        """
        raise NotImplementedError

    # -*- Overloaded built-in -*-

    @abstractmethod
    def __iter__(self) -> Generator[IndOrT, None, None]:
        """Iterate on oriented nodes.

        Yields
        ------
        IndOrT
            Oriented nodes
        """
        raise NotImplementedError

    @abstractmethod
    def __contains__(self, node: IndOrT) -> bool:
        """Return True if oriented node in container, else False.

        Returns
        -------
        bool
            True if oriented node is in container, else False
        """
        raise NotImplementedError

    @abstractmethod
    def __len__(self) -> int:
        """Return the number of oriented nodes.

        Returns
        -------
        int
            Number of oriented nodes
        """
        raise NotImplementedError
