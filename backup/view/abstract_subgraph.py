# -*- coding=utf-8 -*-

"""Abstract base class for sub overlaps graphs module."""

from __future__ import annotations

from abc import abstractmethod
from collections.abc import Iterable
from typing import Any

from revsymg.exceptions import NoGraphAttribute
from revsymg.graphs.view.abstract_subedges import _SubEdgesAbstract
from revsymg.graphs.view.abstract_subnodes import _SubNodesAbstract
from revsymg.index_lib import EIndOrIndT, IndOrT


# ============================================================================ #
#                              SUB OVERLAPS GRAPH                              #
# ============================================================================ #
class _SubOvGAbstract():
    """Abstract base class for sub-overlaps graphs."""

    _attributes: dict[str, Any]

    # -*- Graph methods -*-

    def attr(self, attrname: str) -> Any:
        """Return the value associated to attribute `attrname`.

        Parameters
        ----------
        attrname : str
            Attribute name

        Returns
        -------
        Any
            Attribute value corresponding to the attribute name

        Raises
        ------
        NoGraphAttribute
            When there is no attribute key
        """
        try:
            return self._attributes[attrname]
        except KeyError as exc:
            raise NoGraphAttribute(attrname) from exc

    def attrs(self) -> dict[str, Any]:
        """Return the dictionnary of all attributes.

        Returns
        -------
        dict
            Dictionnary of attribute name as key and their value
        """
        return self._attributes

    # -*- Subgraph methods -*-

    @abstractmethod
    def subgraph_from_nodes(self, nodes: Iterable[IndOrT]) -> _SubOvGAbstract:
        """Return a sub overlaps graph from nodes.

        Parameters
        ----------
        nodes : iterable of IndOrT
            An iterable of nodes

        Returns
        -------
        _SubOvGAbstract
            The overlaps graph masked by nodes
        """
        raise NotImplementedError

    @abstractmethod
    def subgraph_from_edges(self,
                            edges: Iterable[EIndOrIndT]) -> _SubOvGAbstract:
        """Return a sub overlaps graph from edges.

        Parameters
        ----------
        edges : iterable of EIndOrIndT
            An iterable of edges

        Returns
        -------
        _SubOvGAbstract
            The overlaps graph masked by edges
        """
        raise NotImplementedError

    # -*- Getter -*-

    @abstractmethod
    def nodes(self) -> _SubNodesAbstract:
        """Return the nodes container.

        Returns
        -------
        _SubNodesAbstract
            Sub nodes container
        """
        raise NotImplementedError

    @abstractmethod
    def edges(self) -> _SubEdgesAbstract:
        """Return the edge container.

        Returns
        -------
        _SubEdgesAbstract
            Sub edges container
        """
        raise NotImplementedError
