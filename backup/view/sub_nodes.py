# -*- coding=utf-8 -*-

"""Sub nodes container view class module."""

from __future__ import annotations

from collections.abc import Generator
from typing import Any

from bitarray import bitarray

from revsymg.graphs.attributes import AttributesContainer
from revsymg.graphs.revsym_nodes import Nodes
from revsymg.graphs.view.abstract_subnodes import _SubNodesAbstract
from revsymg.index_lib import FORWARD_INT, IND, OR, REVERSE_INT, IndOrT


# ============================================================================ #
#                                     TYPE                                     #
# ============================================================================ #
NodesMasksT = tuple[bitarray, bitarray]


# ============================================================================ #
#                              SUB NODES CONTAINER                             #
# ============================================================================ #
class SubNodes(_SubNodesAbstract):
    """The SubNodes concrete class.

    You must not call this class.
    Use it only for typing or get an already existing object.
    """

    def __init__(self, _nodes: Nodes):
        """The Initializer."""
        self._nodes: Nodes = _nodes
        # _mask[v.or][v.ind] = True if v in V_sub
        card_v: int = len(self._nodes)
        self._mask: NodesMasksT = (
            bitarray('0') * card_v,
            bitarray('0') * card_v,
        )

    # -*- Index -*-

    def card_index(self) -> int:
        """Return the number of nodes' indices.

        Returns
        -------
        int
            Number of nodes' indices
        """
        m_mask = self._mask[FORWARD_INT] | self._mask[REVERSE_INT]
        return m_mask.count()

    def card_indors(self) -> int:
        """Return the number of oriented nodes.

        Returns
        -------
        int
            The number of oriented nodes
        """
        return self._mask[FORWARD_INT].count() + self._mask[REVERSE_INT].count()

    # -*- Attributes -*-

    def attr(self, node: IndOrT, attrname: str) -> Any:
        """Return the value associated to `node` attribute `attrname`.

        Parameters
        ----------
        node : IndOrT
            Oriented node
        attrname : str
            Attribute name

        Returns
        -------
        Any
            Attribute value corresponding to the attribute name

        Raises
        ------
        NoVertexAttribute
            If node has no attribute name like that
        """
        return self._nodes.attr(node, attrname)

    def attrs(self, node: IndOrT) -> dict[str, Any]:
        """Return the dictionnary of all `node`'s attributes.

        Parameters
        ----------
        node : IndOrT
            Oriented node

        Returns
        -------
        dict
            Dictionnary of attribute name as key and their value
        """
        # OPTIMIZE: generator instead of dict
        return self._nodes.attrs(node)

    # -*- Getter -*-

    def card_v(self) -> int:
        """Return the number of oriented nodes.

        Returns
        -------
        int
            Number of oriented nodes
        """
        return self._nodes.card_v()

    def nodes(self) -> Nodes:
        """Return original nodes container.

        Returns
        -------
        Nodes
            Nodes container
        """
        return self._nodes

    def attributes(self) -> AttributesContainer:
        """Return attributes container.

        Returns
        -------
        AttributesContainer
            Attribute container
        """
        return self._nodes.attributes()

    def forward_mask(self) -> bitarray:
        """Return forward mask object attribute.

        Returns
        -------
        bitarray
            Forward mask
        """
        return self._mask[FORWARD_INT]

    def reverse_mask(self) -> bitarray:
        """Return reverse mask object attribute.

        Returns
        -------
        bitarray
            Reverse mask
        """
        return self._mask[REVERSE_INT]

    # -*- Setter -*-

    def add_to_mask(self, node: IndOrT):
        """Add oriented node to mask.

        Parameters
        ----------
        node: IndOrT
            Oriented node
        """
        self._mask[node[OR]][node[IND]] = True

    def combine(self, nodes: SubNodes):
        """Combine the self masks with `nodes` ones.

        Parameters
        ----------
        nodes: SubNodes
            Sub nodes container
        """
        forward_mask = self._mask[FORWARD_INT]
        forward_mask &= nodes.forward_mask()
        reverse_mask = self._mask[REVERSE_INT]
        reverse_mask &= nodes.reverse_mask()

    # -*- Overloaded built-in -*-

    def __iter__(self) -> Generator[IndOrT, None, None]:
        """Iterate on oriented nodes.

        Yields
        ------
        IndOrT
            Oriented nodes
        """
        for orientation in (FORWARD_INT, REVERSE_INT):
            for ind, is_in in enumerate(self._mask[orientation]):
                if is_in:
                    yield ind, orientation

    def __contains__(self, node: IndOrT) -> bool:
        """Return True if oriented node in container, else False.

        Returns
        -------
        bool
            True if oriented node is in container, else False
        """
        try:
            return bool(self._mask[node[OR]][node[IND]])
        except IndexError:
            return False

    def __len__(self) -> int:
        """Return the number of oriented nodes.

        Returns
        -------
        int
            Number of oriented nodes
        """
        return self._mask[FORWARD_INT].count() + self._mask[REVERSE_INT].count()
