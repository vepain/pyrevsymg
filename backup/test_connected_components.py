# -*- coding=utf-8 -*-

"""File for testing Graph Algorithms."""

# pylint: disable=redefined-outer-name, compare-to-zero, missing-param-doc


from revsymg import (
    FORWARD_INT,
    IND,
    OR,
    ORIENT_REV,
    REVERSE_INT,
    IndexErrorCnntedComp,
    RevSymGraph,
    connected_comp_to_overlaps_graph,
    connected_components,
    nonredundant_connected_components,
)


# ============================================================================ #
#                                     TESTS                                    #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                             Connected Components                             #
# ---------------------------------------------------------------------------- #
def test_cc(ovg: RevSymGraph):
    """Test connected component."""
    ccompcont = connected_components(ovg)
    assert ccompcont.card_cc() == 4
    tl_cc_want = (
        (0, 0, 1, 0, 2, 3),
        (1, 1, 0, 1, 3, 2),
    )
    for v in ovg.nodes():
        assert ccompcont[v] == tl_cc_want[v[OR]][v[IND]]
        assert ccompcont.reverse_cc(v) == tl_cc_want[ORIENT_REV[v[OR]]][v[IND]]
    for index, (_, v_cc, _, v_rev_cc) in enumerate(ccompcont):
        assert v_cc == tl_cc_want[FORWARD_INT][index]
        assert v_rev_cc == tl_cc_want[REVERSE_INT][index]

    assert not ccompcont.merged_orientations((0, FORWARD_INT))


def test_cc_revcmp(ovg_revcmp: RevSymGraph):
    """Test connected componenent."""
    ccompcont = connected_components(ovg_revcmp)
    assert ccompcont.card_cc() == 3
    tl_cc_want = (
        [0, 0, 0, 0, 1, 2],
        [0, 0, 0, 0, 2, 1],
    )
    for v in ovg_revcmp.nodes():
        assert ccompcont[v] == tl_cc_want[v[OR]][v[IND]]
    assert ccompcont.merged_orientations((0, FORWARD_INT))


def test_cc_subgraph(ovg: RevSymGraph):
    """Test on find CC in subgraph."""
    nodes = (
        (1, REVERSE_INT), (0, REVERSE_INT), (2, FORWARD_INT), (3, REVERSE_INT),
    )
    sub = ovg.subgraph_from_nodes(nodes)  # type: ignore
    ccompcont = connected_components(sub)
    assert ccompcont.card_cc() == 1
    edges = (
        ((1, REVERSE_INT), (0, REVERSE_INT)),
        ((2, FORWARD_INT), (1, REVERSE_INT)),
        ((3, REVERSE_INT), (2, FORWARD_INT)),
    )
    sub = ovg.subgraph_from_edges(
        [
            (edge, ovg.edges().eindor_to_eind(edge))
            for edge in edges
        ],
    )  # type: ignore
    ccompcont = connected_components(sub)
    assert ccompcont.card_cc() == 1


def test_nreddt_cc_simple(ovg: RevSymGraph):
    """Test nonredundant cc algo."""
    ccompcont = connected_components(ovg)
    l_ccs = nonredundant_connected_components(ccompcont)
    assert len(l_ccs) == 2
    ccomp = l_ccs[0]
    assert ccomp.cc_id() == 0
    ccomp.set_cc_id(42)
    assert ccomp.cc_id() == 42
    assert len(list(ccomp.edges(ovg))) == 3
    assert len(list(ccomp.nonredundant_edges(ovg))) == 3
    assert len(ccomp) == 4
    nodes = ((0, 0), (1, 0), (2, 1), (3, 0))
    for node in ccomp:
        assert node in nodes
    assert ccomp[0] == (0, 0)
    try:
        print(ccomp[42])
    except IndexErrorCnntedComp:
        assert True


def test_nreddt_cc_double(ovg_revcmp: RevSymGraph):
    """Test nonredundant cc algo."""
    ccompcont = connected_components(ovg_revcmp)
    l_ccs = nonredundant_connected_components(ccompcont)
    assert len(l_ccs) == 2
    ccomp = l_ccs[0]
    assert len(list(ccomp.edges(ovg_revcmp))) == 8
    assert len(list(ccomp.nonredundant_edges(ovg_revcmp))) == 4
    assert len(ccomp) == 4
    nodes = (
        (0, 0), (1, 0), (2, 0), (3, 0),
        (0, 1), (1, 1), (2, 1), (3, 1),
    )
    for node in ccomp:
        assert node in nodes
    assert ccomp[0] == (0, 0)
    try:
        print(ccomp[42])
    except IndexErrorCnntedComp:
        assert True


def test_ccomp_ovg(ovg: RevSymGraph):
    """Test connected componenent overlaps graph."""
    ccompcont = connected_components(ovg)
    ovg_ccs = connected_comp_to_overlaps_graph(ovg, ccompcont)
    assert len(ovg_ccs) == 2
    ovg1, ovg2 = ovg_ccs  # pylint: disable=unbalanced-tuple-unpacking
    assert ovg1.card_reads() == 4
    assert ovg1.card_overlaps() == 3
    assert ovg2.card_reads() == 2
    assert ovg2.card_overlaps() == 1


def test_ccomp_ovg_revcomp(ovg_revcmp: RevSymGraph):
    """Test connected componenent overlaps graph."""
    ccompcont = connected_components(ovg_revcmp)
    ovg_ccs = connected_comp_to_overlaps_graph(ovg_revcmp, ccompcont)
    assert len(ovg_ccs) == 2
    ovg1, ovg2 = ovg_ccs  # pylint: disable=unbalanced-tuple-unpacking
    assert ovg1.card_reads() == 4
    assert ovg1.card_overlaps() == 4
    assert ovg2.card_reads() == 2
    assert ovg2.card_overlaps() == 1
