# -*- coding=utf-8 -*-

"""File for testing Graph Algorithms."""

# pylint: disable=redefined-outer-name, compare-to-zero, missing-param-doc


from revsymg import RevSymGraph, transitive_reduction
from tests.lib_tests import OVLEN, RLEN


# ============================================================================ #
#                                     TESTS                                    #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                             Transitive Reduction                             #
# ---------------------------------------------------------------------------- #
def test_no_reduce(ovg: RevSymGraph):
    """Test transitive reduction with no reduction."""
    n_reads = ovg.card_reads()
    n_edges = ovg.card_overlaps()
    transitive_reduction(ovg, read_len_str=RLEN, ov_len_str=OVLEN)
    assert ovg.card_reads() == n_reads
    assert ovg.card_overlaps() == n_edges


def test_reduce(ovg_trans: RevSymGraph):
    """Test transitive reduction with reductions."""
    n_reads = ovg_trans.card_reads()
    transitive_reduction(ovg_trans, read_len_str=RLEN, ov_len_str=OVLEN)
    assert ovg_trans.card_reads() == n_reads
    assert ovg_trans.card_overlaps() == 6
