# -*- coding=utf-8 -*-

"""File for testing Graph IO."""

# pylint: disable=redefined-outer-name, compare-to-zero, missing-param-doc

from pickle import dump as pickle_dump

from revsymg import (
    RevSymGraph,
    ovg_stats,
    ovg_to_graphml,
    ovg_to_pickle,
    pickle_to_ovg,
    rm,
)
from tests.lib_tests import TMP_OUTPUT_PATH, TMP_PICKLE_PATH


# ============================================================================ #
#                                     TESTS                                    #
# ============================================================================ #
def test_graphml(ovg: RevSymGraph):
    """Write a graphml."""
    TMP_OUTPUT_PATH.mkdir(exist_ok=True)
    graph_path = TMP_OUTPUT_PATH / 'og.graphml'
    ovg_to_graphml(ovg, graph_path)
    assert graph_path.exists()
    rm(TMP_OUTPUT_PATH)


def test_pickle(ovg: RevSymGraph):
    """Test pickle."""
    TMP_OUTPUT_PATH.mkdir(exist_ok=True)
    ovg_to_pickle(ovg, TMP_PICKLE_PATH)
    assert True
    pickle_to_ovg(TMP_PICKLE_PATH)
    assert True
    rm(TMP_PICKLE_PATH)
    with open(TMP_PICKLE_PATH, 'wb') as false_ovg:
        pickle_dump(TMP_PICKLE_PATH, false_ovg)
    try:
        pickle_to_ovg(TMP_PICKLE_PATH)
    except TypeError:
        assert True
    rm(TMP_OUTPUT_PATH)


def test_stats(ovg: RevSymGraph):
    """Test ovg stat string."""
    str_stats = ovg_stats(ovg)
    str_wanted = ('|   reads   |  overlaps  | subsequences |\n'
                  '|:---------:|:----------:|:------------:|\n'
                  f'| {ovg.card_reads():<9} |'
                  f' {ovg.card_overlaps():<10} |'
                  f" {'None':<12} |\n\n"
                  '|   vertices   |  edges  |\n'
                  '|:------------:|:-------:|\n'
                  f'| {ovg.nodes().card_v():<12} |'
                  f' {ovg.edges().card_e():<7} |\n')
    assert str_stats == str_wanted
