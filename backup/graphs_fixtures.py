# -*- coding=utf-8 -*-

"""File for testing Graph."""

# pylint: disable=redefined-outer-name, compare-to-zero,
# pylint: disable=missing-param-doc, missing-yield-doc

from collections.abc import Generator

import pytest

from revsymg import RevSymGraph, SubRevSymGraph
from tests.lib_tests import (
    OVERLAPS,
    OVERLAPS_INVCMP,
    OVERLAPS_TRANS,
    SUB_EDGES,
    fill_ovg,
)


# ============================================================================ #
#                                   FIXTURES                                   #
# ============================================================================ #
@pytest.fixture
def empty_ovg() -> Generator[RevSymGraph, None, None]:
    """Yield empty overlaps graph."""
    # Arrange
    empty_ovg = RevSymGraph()
    yield empty_ovg
    # Cleanup


@pytest.fixture
def ovg(empty_ovg: RevSymGraph) -> Generator[RevSymGraph, None, None]:
    """Yield complete minimal overlaps graph.

    u_f -> v_f
    v_f -> w_r
    w_r -> x_f

    y_f -> z_r

    -----> u                            -------> y
       ----------> v                       <------- z
               <-------- w
                      --------> x
    """
    # Arrange
    fill_ovg(empty_ovg, OVERLAPS)
    yield empty_ovg
    # Cleanup


@pytest.fixture
def ovg_revcmp(ovg: RevSymGraph) -> Generator[RevSymGraph, None, None]:
    """Yield complete minimal overlaps graph suffering from inv repeat.

    u_f -> v_f
    v_f -> w_r
    w_r -> x_f

    y_f -> z_r

    -----> u                            -------> y
       ----------> v                       <------- z
               <-------- w
                      --------> x
    And

    x_f -> v_r

    --------> x
          <---------- v
    """
    # Arrange
    fill_ovg(ovg, OVERLAPS_INVCMP)
    yield ovg
    # Cleanup


@pytest.fixture
def ovg_trans(ovg: RevSymGraph) -> Generator[RevSymGraph, None, None]:
    """Yield complete minimal overlaps graph.

    u_f -> v_f
    v_f -> w_r
    w_r -> x_f

    y_f -> z_r

    -----> u                            -------> y
       ----------> v                       <------- z
               <-------- w
                      --------> x
    And

    -----> u
     ------> a
       ----------> v
              ------> b
               <-------- w
    """
    # Arrange
    fill_ovg(ovg, OVERLAPS_TRANS)
    yield ovg
    # Cleanup


@pytest.fixture
def subovg(ovg: RevSymGraph) -> Generator[SubRevSymGraph, None, None]:
    """Yield suboverlaps graph."""
    # Arrange
    subovg = ovg.subgraph_from_edges(
        [
            (edge, ovg.edges().eindor_to_eind(edge))
            for edge in SUB_EDGES
        ],
    )
    yield subovg
