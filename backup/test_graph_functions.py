# -*- coding=utf-8 -*-

"""File for testing Graph Algorithms."""

# pylint: disable=redefined-outer-name, compare-to-zero, missing-param-doc


from revsymg import (
    FORWARD_INT,
    REVERSE_INT,
    RevSymGraph,
    SubRevSymGraph,
    degree,
    in_degree,
    out_degree,
)


# ============================================================================ #
#                                     TESTS                                    #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                    Degrees                                   #
# ---------------------------------------------------------------------------- #
def test_degree(ovg: RevSymGraph):
    """Verify degree method."""
    node = (1, FORWARD_INT)
    assert degree(ovg, node) == 2  # type: ignore


def test_in_degree(ovg: RevSymGraph):
    """Test in-degree."""
    node = (1, FORWARD_INT)
    assert in_degree(ovg, node) == 1  # type: ignore


def test_out_degree(ovg: RevSymGraph):
    """Test in-degree."""
    node = (1, FORWARD_INT)
    assert out_degree(ovg, node) == 1  # type: ignore


def test_sub_degree(subovg: SubRevSymGraph):
    """Verify degree method."""
    node = (0, REVERSE_INT)
    assert degree(subovg, node) == 1  # type: ignore


def test_sub_in_degree(subovg: SubRevSymGraph):
    """Test in-degree."""
    node = (0, REVERSE_INT)
    assert in_degree(subovg, node) == 1  # type: ignore


def test_sub_out_degree(subovg: SubRevSymGraph):
    """Test in-degree."""
    node = (0, REVERSE_INT)
    assert out_degree(subovg, node) == 0  # type: ignore
