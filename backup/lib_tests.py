# -*- coding=utf-8 -*-

"""Library for tests."""

import os
from collections.abc import Iterable
from pathlib import Path
from typing import Any

from revsymg import FORWARD_INT, REVERSE_INT, EIndOrT, RevSymGraph


# pylint: disable=missing-param-doc

# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                     Data                                     #
# ---------------------------------------------------------------------------- #
MULT = 1000
READS_LEN = {
    'u': len('----->') * MULT,
    'v': len('---------->') * MULT,
    'w': len('<--------') * MULT,
    'x': len('-------->') * MULT,
    'y': len('------->') * MULT,
    'z': len('<-------') * MULT,
    'a': len('------>') * MULT,
    'b': len('------>') * MULT,
}
OVERLAPS = [
    ('u', 'v', len('-->') * MULT, '+', 0),
    ('v', 'w', len('-->') * MULT, '-', 0),
    ('w', 'v', len('<--') * MULT, '-', 0),
    ('w', 'x', len('--') * MULT, '-', 1),
    ('z', 'y', len('<----') * MULT, '-', 0),
]
OVERLAPS_INVCMP = [
    ('x', 'v', len('-->') * MULT, '-', 0),
]
OVERLAPS_TRANS = [
    ('u', 'a', len('---->') * MULT, '+', 0),
    ('a', 'v', len('---->') * MULT, '+', 0),
    ('v', 'b', len('--->') * MULT, '+', 0),
    ('b', 'w', len('----->') * MULT, '-', 0),
    ('w', 'b', len('<-----') * MULT, '-', 0),
]

STRAND_STR_INT = {
    '+': FORWARD_INT,
    '-': REVERSE_INT,
}
SUB_EDGES = (
    ((1, REVERSE_INT), (0, REVERSE_INT)),
    ((2, FORWARD_INT), (1, REVERSE_INT)),
    ((3, REVERSE_INT), (2, FORWARD_INT)),
)
SUB_NODES = (
    (1, REVERSE_INT),
    (0, REVERSE_INT),
    (2, FORWARD_INT),
    (3, REVERSE_INT),
)

# ---------------------------------------------------------------------------- #
#                                  Attributes                                  #
# ---------------------------------------------------------------------------- #
# Reads
RLEN = 'read_length'
# Overlaps
OVLEN = 'overlap_length'

# ---------------------------------------------------------------------------- #
#                                     Paths                                    #
# ---------------------------------------------------------------------------- #
# Graph
TMP_OUTPUT_PATH = Path(os.path.dirname(__file__)) / 'tmp'
TMP_PICKLE_PATH = TMP_OUTPUT_PATH / 'ovg.pickle'


# ============================================================================ #
#                                   FUNCTIONS                                  #
# ============================================================================ #
def fill_ovg(ovg_to_fill: RevSymGraph, paf_lines: Iterable[Any]):
    """Fill the overlaps graph."""
    for uid, vid, ovlen, strand, ovway in paf_lines:
        u = ovg_to_fill.add_read(
            uid,
            orientation=FORWARD_INT,
            **{RLEN: READS_LEN[uid]},
        )
        v = ovg_to_fill.add_read(
            vid,
            STRAND_STR_INT[strand],
            **{RLEN: READS_LEN[vid]},
        )
        edge: EIndOrT
        if ovway == FORWARD_INT:  # u v
            edge = (u, v)
        else:  # v u
            edge = (v, u)
        ovg_to_fill.add_overlap(edge, **{OVLEN: ovlen})
