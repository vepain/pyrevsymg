# -*- coding=utf-8 -*-

"""Tests for connected components tools module."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc

from typing import Iterator

import pytest

from revsymg.algorithms.connected_components import connected_components
from revsymg.graphs.revsym_graph import RevSymGraph
from tests.datatest import (
    ATTR_OVLEN,
    OVERLAPS_CC1,
    OVERLAPS_CC1_CC2,
    OVERLAPS_INVCMP_CC1,
    U_F,
    U_R,
    V_F,
    V_R,
    W_F,
    W_R,
    X_F,
    X_R,
    Y_F,
    Y_R,
    Z_F,
    Z_R,
    add_overlap_to_graph,
)


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def revsym_graph_cc1() -> Iterator[RevSymGraph]:
    """Yield overlaps graph with cc1 config."""
    _revsym_graph_cc1 = RevSymGraph()
    _revsym_graph_cc1.edges().new_attr(ATTR_OVLEN, 0)
    for overlap in OVERLAPS_CC1:
        add_overlap_to_graph(_revsym_graph_cc1, overlap)
    yield _revsym_graph_cc1


@pytest.fixture
def revsym_graph_cc1_cc2() -> Iterator[RevSymGraph]:
    """Yield overlaps graph with cc1_cc2 config."""
    _revsym_graph_cc1_cc2 = RevSymGraph()
    _revsym_graph_cc1_cc2.edges().new_attr(ATTR_OVLEN, 0)
    for overlap in OVERLAPS_CC1_CC2:
        add_overlap_to_graph(_revsym_graph_cc1_cc2, overlap)
    yield _revsym_graph_cc1_cc2


@pytest.fixture
def revsym_graph_invcmp_cc1() -> Iterator[RevSymGraph]:
    """Yield overlaps graph with invcmp_cc1 config."""
    _revsym_graph_invcmp_cc1 = RevSymGraph()
    _revsym_graph_invcmp_cc1.edges().new_attr(ATTR_OVLEN, 0)
    for overlap in OVERLAPS_INVCMP_CC1:
        add_overlap_to_graph(_revsym_graph_invcmp_cc1, overlap)
    yield _revsym_graph_invcmp_cc1


# ============================================================================ #
#                                TEST FUNCTIONS                                #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                             Connected_components                             #
# ---------------------------------------------------------------------------- #
def test_cc_cc1(revsym_graph_cc1: RevSymGraph):
    """Test connected_component func for cc1."""
    revsym_ccids = connected_components(revsym_graph_cc1)
    assert revsym_ccids[U_F] == 0
    assert revsym_ccids[U_R] == 1
    assert revsym_ccids[V_F] == 0
    assert revsym_ccids[V_R] == 1
    assert revsym_ccids[W_F] == 1
    assert revsym_ccids[W_R] == 0
    assert revsym_ccids[X_F] == 0
    assert revsym_ccids[X_R] == 1


def test_cc_cc1_cc2(revsym_graph_cc1_cc2: RevSymGraph):
    """Test connected_component func for cc1_cc2."""
    revsym_ccids = connected_components(revsym_graph_cc1_cc2)
    assert revsym_ccids[U_F] == 0
    assert revsym_ccids[U_R] == 1
    assert revsym_ccids[V_F] == 0
    assert revsym_ccids[V_R] == 1
    assert revsym_ccids[W_F] == 1
    assert revsym_ccids[W_R] == 0
    assert revsym_ccids[X_F] == 0
    assert revsym_ccids[X_R] == 1
    assert revsym_ccids[Y_F] == 2
    assert revsym_ccids[Y_R] == 3
    assert revsym_ccids[Z_F] == 3
    assert revsym_ccids[Z_R] == 2


def test_cc_invcmp_cc1(revsym_graph_invcmp_cc1: RevSymGraph):
    """Test connected_component func for invcmp_cc1."""
    revsym_ccids = connected_components(revsym_graph_invcmp_cc1)
    assert revsym_ccids[U_F] == 0
    assert revsym_ccids[U_R] == 0
    assert revsym_ccids[V_F] == 0
    assert revsym_ccids[V_R] == 0
    assert revsym_ccids[W_F] == 0
    assert revsym_ccids[W_R] == 0
    assert revsym_ccids[X_F] == 0
    assert revsym_ccids[X_R] == 0
