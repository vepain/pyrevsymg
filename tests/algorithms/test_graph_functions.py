# -*- coding=utf-8 -*-

"""Tests for graph functions module."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc


from pathlib import Path

import pytest

from revsymg.algorithms.graph_functions import degree, in_degree, out_degree
from revsymg.exceptions import NoVertexIndex
from revsymg.graphs.revsym_graph import RevSymGraph
from revsymg.index_lib import FORWARD_INT, IND, OR
from tests.datatest import (
    U_F,
    U_R,
    UV_IND,
    V_F,
    V_R,
    VW_IND,
    W_F,
    W_R,
    WX_IND,
    X_F,
    X_R,
)
from tests.graphs.test_attributes import ATTR_DEF, ATTR_NAME, ATTR_VAL
from tests.graphs.test_revsym_vertices import NO_INDEX


# ============================================================================ #
#                                TEST FUNCTIONS                                #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                   In_degree                                  #
# ---------------------------------------------------------------------------- #
def test_in_degree_exc(revsym_graph_all: RevSymGraph):
    """Test in degree meth for exc."""
    with pytest.raises(NoVertexIndex):
        in_degree(revsym_graph_all, (NO_INDEX, FORWARD_INT))


def test_in_degree_ok(revsym_graph_all: RevSymGraph):
    """Test in degree meth with all edge types."""
    assert in_degree(revsym_graph_all, U_F) == 0
    assert in_degree(revsym_graph_all, U_R) == 1
    assert in_degree(revsym_graph_all, V_F) == 1
    assert in_degree(revsym_graph_all, V_R) == 1
    assert in_degree(revsym_graph_all, W_F) == 1
    assert in_degree(revsym_graph_all, W_R) == 1
    assert in_degree(revsym_graph_all, X_F) == 1
    assert in_degree(revsym_graph_all, X_R) == 0


# ---------------------------------------------------------------------------- #
#                                  Out_degree                                  #
# ---------------------------------------------------------------------------- #
def test_out_degree_exc(revsym_graph_all: RevSymGraph):
    """Test out degree meth for exc."""
    with pytest.raises(NoVertexIndex):
        out_degree(revsym_graph_all, (NO_INDEX, FORWARD_INT))


def test_out_degree_ok(revsym_graph_all: RevSymGraph):
    """Test out degree meth with all edge types."""
    assert out_degree(revsym_graph_all, U_F) == 1
    assert out_degree(revsym_graph_all, U_R) == 0
    assert out_degree(revsym_graph_all, V_F) == 1
    assert out_degree(revsym_graph_all, V_R) == 1
    assert out_degree(revsym_graph_all, W_F) == 1
    assert out_degree(revsym_graph_all, W_R) == 1
    assert out_degree(revsym_graph_all, X_F) == 0
    assert out_degree(revsym_graph_all, X_R) == 1


# ---------------------------------------------------------------------------- #
#                                    Degree                                    #
# ---------------------------------------------------------------------------- #
def test_degree_exc(revsym_graph_all: RevSymGraph):
    """Test out degree meth for exc."""
    with pytest.raises(NoVertexIndex):
        degree(revsym_graph_all, (NO_INDEX, FORWARD_INT))


def test_degree_ok(revsym_graph_all: RevSymGraph):
    """Test degree meth with all edge types."""
    assert degree(revsym_graph_all, U_F) == 1
    assert degree(revsym_graph_all, U_R) == 1
    assert degree(revsym_graph_all, V_F) == 2
    assert degree(revsym_graph_all, V_R) == 2
    assert degree(revsym_graph_all, W_F) == 2
    assert degree(revsym_graph_all, W_R) == 2
    assert degree(revsym_graph_all, X_F) == 1
    assert degree(revsym_graph_all, X_R) == 1
