# -*- coding=utf-8 -*-

"""Connected component identifiers class tests module."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc

from typing import Iterator

import pytest

from revsymg.algorithms.connected_components.cnnted_comp_id import (
    RevSymCCId,
    _RevSymCCIdFactory,
)
from revsymg.exceptions import NoConnectedComponentId, NoVertexIndex
from revsymg.index_lib import FORWARD_INT, REVERSE_INT


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
__CARD_INDEX = 4


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                              _RevSymCCIdFactory                              #
# ---------------------------------------------------------------------------- #
@pytest.fixture
def revsym_cc_f() -> Iterator[_RevSymCCIdFactory]:
    """Yields an empty connected component identifiers container."""
    _revsym_cc_f = _RevSymCCIdFactory(__CARD_INDEX)
    yield _revsym_cc_f


@pytest.fixture
def revsym_cc_f_1() -> Iterator[_RevSymCCIdFactory]:
    """Yields an one connected component identifiers container."""
    _revsym_cc_f_1 = _RevSymCCIdFactory(__CARD_INDEX)
    _revsym_cc_f_1._nb_distinct = 2
    _revsym_cc_f_1._ccompids_cur = (0, 1)
    _revsym_cc_f_1._ccompid_ind = 0
    _revsym_cc_f_1._cc_couples = [(0, 1)]
    _revsym_cc_f_1._f_vertices_cc = [
        None, None, None, None,
    ]
    yield _revsym_cc_f_1


@pytest.fixture
def revsym_cc_f_split() -> Iterator[_RevSymCCIdFactory]:
    """Yields a cc id container when strands are splitted."""
    _revsym_cc_f_split = _RevSymCCIdFactory(__CARD_INDEX)
    _revsym_cc_f_split._nb_distinct = 2
    _revsym_cc_f_split._ccompids_cur = (0, 1)
    _revsym_cc_f_split._ccompid_ind = 0
    _revsym_cc_f_split._cc_couples = [(0, 1)]
    _revsym_cc_f_split._f_vertices_cc = [
        (0, REVERSE_INT), None, None, (0, FORWARD_INT),
    ]
    yield _revsym_cc_f_split


@pytest.fixture
def revsym_cc_f_merge() -> Iterator[_RevSymCCIdFactory]:
    """Yields a cc id container when strands are merged."""
    _revsym_cc_f_merge = _RevSymCCIdFactory(__CARD_INDEX)
    _revsym_cc_f_merge._nb_distinct = 1
    _revsym_cc_f_merge._ccompids_cur = (0, 0)
    _revsym_cc_f_merge._ccompid_ind = 0
    _revsym_cc_f_merge._cc_couples = [(0, 0)]
    _revsym_cc_f_merge._f_vertices_cc = [
        (0, REVERSE_INT), None, None, (0, FORWARD_INT),
    ]
    yield _revsym_cc_f_merge


# ---------------------------------------------------------------------------- #
#                                  RevSymCCId                                  #
# ---------------------------------------------------------------------------- #
@pytest.fixture
def revsym_cc() -> Iterator[RevSymCCId]:
    """Yields an empty connected component identifiers container."""
    _revsym_cc = RevSymCCId(
        0,
        [],
        [None, None, None, None],
    )
    yield _revsym_cc


@pytest.fixture
def revsym_cc_1() -> Iterator[RevSymCCId]:
    """Yields an one connected component identifiers container."""
    _revsym_cc_1 = RevSymCCId(
        2,
        [(0, 1)],
        [None, None, None, None],
    )
    yield _revsym_cc_1


@pytest.fixture
def revsym_cc_split() -> Iterator[RevSymCCId]:
    """Yields a cc id container when strands are splitted."""
    _revsym_cc_split = RevSymCCId(
        2,
        [(0, 1)],
        [(0, REVERSE_INT), None, None, (0, FORWARD_INT)],
    )
    yield _revsym_cc_split


@pytest.fixture
def revsym_cc_merge() -> Iterator[RevSymCCId]:
    """Yields a cc id container when strands are merged."""
    _revsym_cc_merge = RevSymCCId(
        1,
        [(0, 0)],
        [(0, REVERSE_INT), None, None, (0, FORWARD_INT)],
    )
    yield _revsym_cc_merge


# ============================================================================ #
#                       _REVSYMCCIDFACTORY TEST FUNCTIONS                      #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                   __init__                                   #
# ---------------------------------------------------------------------------- #
def test_revsym_cc_f_init(revsym_cc_f: _RevSymCCIdFactory):
    """Test __init__ for builder."""
    assert revsym_cc_f._nb_distinct == 0
    assert revsym_cc_f._ccompids_cur == (0, 1)
    assert revsym_cc_f._ccompid_ind == -1
    assert revsym_cc_f._cc_couples == []
    assert revsym_cc_f._f_vertices_cc == [None, None, None, None]


# ---------------------------------------------------------------------------- #
#                                 New_cc_couple                                #
# ---------------------------------------------------------------------------- #
def test_new_cc_couple(revsym_cc_f: _RevSymCCIdFactory):
    """Test new_cc_couple."""
    revsym_cc_f.new_cc_couple()
    assert revsym_cc_f._nb_distinct == 2
    assert revsym_cc_f._ccompids_cur == (0, 1)
    assert revsym_cc_f._ccompid_ind == 0
    assert revsym_cc_f._cc_couples == [(0, 1)]
    assert revsym_cc_f._f_vertices_cc == [None, None, None, None]
    revsym_cc_f.new_cc_couple()
    assert revsym_cc_f._nb_distinct == 4
    assert revsym_cc_f._ccompids_cur == (2, 3)
    assert revsym_cc_f._ccompid_ind == 1
    assert revsym_cc_f._cc_couples == [(0, 1), (2, 3)]
    assert revsym_cc_f._f_vertices_cc == [None, None, None, None]


# ---------------------------------------------------------------------------- #
#                                   Set_cc_id                                  #
# ---------------------------------------------------------------------------- #
def test_set_cc_id_novertexindex(revsym_cc_f: _RevSymCCIdFactory):
    """Test set_cc_id for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_cc_f.set_cc_id((__CARD_INDEX, REVERSE_INT))


def test_set_cc_id_ok(revsym_cc_f_1: _RevSymCCIdFactory):
    """Test set_cc_id when ok."""
    revsym_cc_f_1.set_cc_id((0, REVERSE_INT))
    assert revsym_cc_f_1._f_vertices_cc == [
        (0, REVERSE_INT), None, None, None,
    ]
    revsym_cc_f_1.set_cc_id((3, FORWARD_INT))
    assert revsym_cc_f_1._f_vertices_cc == [
        (0, REVERSE_INT), None, None, (0, FORWARD_INT),
    ]


# ---------------------------------------------------------------------------- #
#                                  Continuity                                  #
# ---------------------------------------------------------------------------- #
def test_continuity_novertexindex(revsym_cc_f_1: _RevSymCCIdFactory):
    """Test continuity for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_cc_f_1.continuity((__CARD_INDEX, FORWARD_INT))


def test_continuity_noconnectedcomponentid(revsym_cc_f_1: _RevSymCCIdFactory):
    """Test continuity for NoConnectedComponentId exc."""
    with pytest.raises(NoConnectedComponentId):
        revsym_cc_f_1.continuity((0, FORWARD_INT))


def test_continuity_split(revsym_cc_f_split: _RevSymCCIdFactory):
    """Test continuity with split."""
    assert revsym_cc_f_split.continuity((3, FORWARD_INT))
    assert not revsym_cc_f_split.continuity((3, REVERSE_INT))


def test_continuity_merge(revsym_cc_f_merge: _RevSymCCIdFactory):
    """Test continuity with merge."""
    assert revsym_cc_f_merge.continuity((3, FORWARD_INT))
    assert revsym_cc_f_merge.continuity((3, REVERSE_INT))


# ---------------------------------------------------------------------------- #
#                                   Merge_cc                                   #
# ---------------------------------------------------------------------------- #
def test_merge_cc(revsym_cc_f_1: _RevSymCCIdFactory):
    """Test merge_cc."""
    revsym_cc_f_1.merge_cc()
    assert revsym_cc_f_1._nb_distinct == 1
    assert revsym_cc_f_1._ccompids_cur == (0, 0)
    assert revsym_cc_f_1._ccompid_ind == 0
    assert revsym_cc_f_1._cc_couples == [(0, 0)]
    assert revsym_cc_f_1._f_vertices_cc == [None, None, None, None]


# ---------------------------------------------------------------------------- #
#                               To_readonly_view                               #
# ---------------------------------------------------------------------------- #
def test_to_readonly_view(revsym_cc_f: _RevSymCCIdFactory):
    """Test to_readonly_view."""
    revsym_cc = revsym_cc_f.to_readonly_view()
    assert isinstance(revsym_cc, RevSymCCId)
    assert revsym_cc._nb_distinct == revsym_cc_f._nb_distinct
    assert revsym_cc._cc_couples == revsym_cc_f._cc_couples
    assert revsym_cc._f_vertices_cc == revsym_cc_f._f_vertices_cc


# ---------------------------------------------------------------------------- #
#                                  __getitem__                                 #
# ---------------------------------------------------------------------------- #
def test_getitem_f_novertexindex(revsym_cc_f_1: _RevSymCCIdFactory):
    """Test __getitem__ for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        _ = revsym_cc_f_1[(__CARD_INDEX, FORWARD_INT)]


def test_getitem_f_none(revsym_cc_f_1: _RevSymCCIdFactory):
    """Test __getitem__ for None."""
    assert revsym_cc_f_1[(0, FORWARD_INT)] is None
    assert revsym_cc_f_1[(0, REVERSE_INT)] is None


def test_getitem_f_split(revsym_cc_f_split: _RevSymCCIdFactory):
    """Test __getitem__ for split."""
    assert revsym_cc_f_split[(0, FORWARD_INT)] == 1
    assert revsym_cc_f_split[(0, REVERSE_INT)] == 0


def test_getitem_f_merge(revsym_cc_f_merge: _RevSymCCIdFactory):
    """Test __getitem__ for merge."""
    assert revsym_cc_f_merge[(0, FORWARD_INT)] == 0
    assert revsym_cc_f_merge[(0, REVERSE_INT)] == 0


# ============================================================================ #
#                           REVSYMCCID TEST FUNCTIONS                          #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                   __init__                                   #
# ---------------------------------------------------------------------------- #
def test_revsym_cc_init(revsym_cc: RevSymCCId):
    """Test __init__ for builder."""
    assert revsym_cc._nb_distinct == 0
    assert revsym_cc._cc_couples == []
    assert revsym_cc._f_vertices_cc == [None, None, None, None]


# ---------------------------------------------------------------------------- #
#                                    Card_cc                                   #
# ---------------------------------------------------------------------------- #
def test_card_cc(revsym_cc: RevSymCCId):
    """Test card_cc when empty."""
    assert revsym_cc.card_cc() == 0


def test_card_cc_split(revsym_cc_split: RevSymCCId):
    """Test card_cc when split."""
    assert revsym_cc_split.card_cc() == 2


def test_card_cc_merge(revsym_cc_merge: RevSymCCId):
    """Test card_cc when merge."""
    assert revsym_cc_merge.card_cc() == 1


# ---------------------------------------------------------------------------- #
#                              Merged_orientations                             #
# ---------------------------------------------------------------------------- #
def test_merged_orientations_novertexindex(revsym_cc: RevSymCCId):
    """Test merged_orientations for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_cc.merged_orientations(__CARD_INDEX)


def test_merged_orientations_noccid(revsym_cc: RevSymCCId):
    """Test merged_orientations for NoConnectedComponentId exc."""
    with pytest.raises(NoConnectedComponentId):
        revsym_cc.merged_orientations(0)


def test_merged_orientations_split(revsym_cc_split: RevSymCCId):
    """Test merged_orientations when split."""
    assert not revsym_cc_split.merged_orientations(0)
    assert not revsym_cc_split.merged_orientations(3)


def test_merged_orientations_merge(revsym_cc_merge: RevSymCCId):
    """Test merged_orientations when merge."""
    assert revsym_cc_merge.merged_orientations(0)
    assert revsym_cc_merge.merged_orientations(3)


# ---------------------------------------------------------------------------- #
#                                  Reverse_cc                                  #
# ---------------------------------------------------------------------------- #
def test_reverse_cc_novertexindex(revsym_cc: RevSymCCId):
    """Test reverse_cc for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_cc.reverse_cc((__CARD_INDEX, FORWARD_INT))


def test_reverse_cc_none(revsym_cc: RevSymCCId):
    """Test reverse_cc for None."""
    assert revsym_cc.reverse_cc((0, FORWARD_INT)) is None
    assert revsym_cc.reverse_cc((0, REVERSE_INT)) is None


def test_reverse_cc_split(revsym_cc_split: RevSymCCId):
    """Test reverse_cc for split."""
    assert revsym_cc_split.reverse_cc((0, FORWARD_INT)) == 0
    assert revsym_cc_split.reverse_cc((0, REVERSE_INT)) == 1
    assert revsym_cc_split.reverse_cc((3, FORWARD_INT)) == 1
    assert revsym_cc_split.reverse_cc((3, REVERSE_INT)) == 0


def test_reverse_cc_merge(revsym_cc_merge: RevSymCCId):
    """Test reverse_cc for merge."""
    assert revsym_cc_merge.reverse_cc((0, FORWARD_INT)) == 0
    assert revsym_cc_merge.reverse_cc((0, REVERSE_INT)) == 0
    assert revsym_cc_merge.reverse_cc((3, FORWARD_INT)) == 0
    assert revsym_cc_merge.reverse_cc((3, REVERSE_INT)) == 0


# ---------------------------------------------------------------------------- #
#                                  __getitem__                                 #
# ---------------------------------------------------------------------------- #
def test_getitem_novertexindex(revsym_cc_1: RevSymCCId):
    """Test __getitem__ for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        _ = revsym_cc_1[(__CARD_INDEX, FORWARD_INT)]


def test_getitem_none(revsym_cc_1: RevSymCCId):
    """Test __getitem__ for None."""
    assert revsym_cc_1[(0, FORWARD_INT)] is None
    assert revsym_cc_1[(0, REVERSE_INT)] is None


def test_getitem_split(revsym_cc_split: RevSymCCId):
    """Test __getitem__ for split."""
    assert revsym_cc_split[(0, FORWARD_INT)] == 1
    assert revsym_cc_split[(0, REVERSE_INT)] == 0


def test_getitem_merge(revsym_cc_merge: RevSymCCId):
    """Test __getitem__ for merge."""
    assert revsym_cc_merge[(0, FORWARD_INT)] == 0
    assert revsym_cc_merge[(0, REVERSE_INT)] == 0


# ---------------------------------------------------------------------------- #
#                                   __iter__                                   #
# ---------------------------------------------------------------------------- #
def test_iter(revsym_cc: RevSymCCId):
    """Test __iter__ when empty."""
    assert (
        tuple(res for res in revsym_cc)
        == (
            ((0, FORWARD_INT), None, (0, REVERSE_INT), None),
            ((1, FORWARD_INT), None, (1, REVERSE_INT), None),
            ((2, FORWARD_INT), None, (2, REVERSE_INT), None),
            ((3, FORWARD_INT), None, (3, REVERSE_INT), None),
        )
    )


def test_iter_split(revsym_cc_split: RevSymCCId):
    """Test __iter__ when split."""
    assert (
        tuple(res for res in revsym_cc_split)
        == (
            ((0, FORWARD_INT), 1, (0, REVERSE_INT), 0),
            ((1, FORWARD_INT), None, (1, REVERSE_INT), None),
            ((2, FORWARD_INT), None, (2, REVERSE_INT), None),
            ((3, FORWARD_INT), 0, (3, REVERSE_INT), 1),
        )
    )


def test_iter_merge(revsym_cc_merge: RevSymCCId):
    """Test __iter__ when merge."""
    assert (
        tuple(res for res in revsym_cc_merge)
        == (
            ((0, FORWARD_INT), 0, (0, REVERSE_INT), 0),
            ((1, FORWARD_INT), None, (1, REVERSE_INT), None),
            ((2, FORWARD_INT), None, (2, REVERSE_INT), None),
            ((3, FORWARD_INT), 0, (3, REVERSE_INT), 0),
        )
    )
