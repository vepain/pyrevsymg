# -*- coding=utf-8 -*-

"""File for testing Graph."""

# flake8: noqa
# pylint: disable=wildcard-import, unused-wildcard-import

from tests.graphs.test_adjancy_table import (
    indices_adj_tab,
    indices_adj_tab_u_v,
    indices_adj_tab_uf_af_vf,
    indices_adj_tab_uf_vf,
    indices_adj_tab_uf_vf_wr,
    indices_adj_tab_uf_vf_wr_xf,
    revsym_adj_tab,
    revsym_adj_tab_u_v,
    revsym_adj_tab_uf_af_vf,
    revsym_adj_tab_uf_vf,
    revsym_adj_tab_uf_vf_wr,
    revsym_adj_tab_uf_vf_wr_xf,
)
from tests.graphs.test_attributes import (
    attr_container,
    attr_container_1,
    attr_container_2,
    attr_container_3,
    attr_container_4,
)
from tests.graphs.test_revsym_egdes import revsym_edges_uf_vf_wr_xf
from tests.graphs.test_revsym_graph import revsym_graph_all
from tests.graphs.test_revsym_vertices import revsym_vertices_uf_vf_wr_xf
