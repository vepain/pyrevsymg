# -*- coding=utf-8 -*-

"""Test imports and import aliases."""

from importlib import import_module
from json import load
from pathlib import Path


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
ALIASES_JSON = Path('docs/src/references/aliases.json')


# ============================================================================ #
#                                TEST FUNCTIONS                                #
# ============================================================================ #
def test_imports():
    """Test imports and napoleon aliases."""
    wrong_aliases: list[str] = []

    with open(ALIASES_JSON, 'r', encoding='utf-8') as json:
        aliases = load(json)

    for name, mod_path in aliases['class'].items():
        try:
            mod = import_module(mod_path)
            getattr(mod, name)
        # pylint: disable=broad-except
        except Exception:  # noqa
            wrong_aliases.append(name)
    for name, mod_path in aliases['data'].items():
        try:
            mod = import_module(mod_path)
            getattr(mod, name)
        # pylint: disable=broad-except
        except Exception:  # noqa
            wrong_aliases.append(name)

    for name in wrong_aliases:
        print(f'[ERROR] `{name}` is not correctly configured')

    assert not wrong_aliases
