# -*- coding=utf-8 -*-

"""Quick usage example code."""

# flake8: noqa: E800

from revsymg.graphs import RevSymGraph
from revsymg.index_lib import FORWARD_INT, REVERSE_INT


def test_quick_usage():
    """Quick usage test."""
    #
    # Create an empty graph
    #
    graph = RevSymGraph()
    vertices = graph.vertices()
    edges = graph.edges()

    #
    # Add two vertices v and w that represents e.g. DNA fragments
    #
    frag_1_index = vertices.add()  # = 0
    frag_2_index = vertices.add()  # = 1

    #
    # Add to the graph the overlap v reverse overlaps w forward
    #
    frag_1_r = (frag_1_index, REVERSE_INT)
    frag_2_f = (frag_2_index, FORWARD_INT)
    overlap_index = edges.add(frag_1_r, frag_2_f)  # = 0

    for u, v, edge_index in edges:
        print(
            f'Predecessor:\t{u}\n'
            f'Successor:\t{v}\n'
            f'Edge index:\t{edge_index}\n',
        )
    # The for-loop print this:
    #
    #   Predecessor:    (1, 1)
    #   Successor:      (0, 0)
    #   Edge index:     0
    #
    #   Predecessor:    (0, 1)
    #   Successor:      (1, 0)
    #   Edge index:     0
    #
