# -*- coding=utf-8 -*-

"""Datatest for pytest."""

from revsymg.graphs.revsym_graph import RevSymGraph
from revsymg.index_lib import FORWARD_INT, REVERSE_INT, IndexT, OrT


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                 Reads Indices                                #
# ---------------------------------------------------------------------------- #
U_IND = 0
V_IND = 1
W_IND = 2
X_IND = 3
Y_IND = 4
Z_IND = 5
A_IND = 4
B_IND = 5
# ---------------------------------------------------------------------------- #
#                               Reads Attributes                               #
# ---------------------------------------------------------------------------- #
MULT = 1000
READS_LEN = {
    U_IND: len('----->') * MULT,
    V_IND: len('---------->') * MULT,
    W_IND: len('<--------') * MULT,
    X_IND: len('-------->') * MULT,
    Y_IND: len('------->') * MULT,
    Z_IND: len('<-------') * MULT,
    A_IND: len('------>') * MULT,
    B_IND: len('------>') * MULT,
}
# ---------------------------------------------------------------------------- #
#                                Oriented Reads                                #
# ---------------------------------------------------------------------------- #
U_F, U_R = (U_IND, FORWARD_INT), (U_IND, REVERSE_INT)
V_F, V_R = (V_IND, FORWARD_INT), (V_IND, REVERSE_INT)
W_F, W_R = (W_IND, FORWARD_INT), (W_IND, REVERSE_INT)
X_F, X_R = (X_IND, FORWARD_INT), (X_IND, REVERSE_INT)
Y_F, Y_R = (Y_IND, FORWARD_INT), (Y_IND, REVERSE_INT)
Z_F, Z_R = (Z_IND, FORWARD_INT), (Z_IND, REVERSE_INT)
A_F, A_R = (A_IND, FORWARD_INT), (A_IND, REVERSE_INT)
B_F, B_R = (B_IND, FORWARD_INT), (B_IND, REVERSE_INT)
# ---------------------------------------------------------------------------- #
#                                   Overlaps                                   #
# ---------------------------------------------------------------------------- #
#
# Type
#
OverlapTestT = tuple[IndexT, OrT, IndexT, OrT, int]
#
# Attribute
#
ATTR_OVLEN = 'overlap_length'
#
# All overlap cases
#
UV_IND = 0
VW_IND = 1
WX_IND = 2

OVERLAPS_CC1: list[OverlapTestT] = [
    (*U_F, *V_F, len('-->') * MULT),
    (*V_F, *W_R, len('-->') * MULT),
    (*W_R, *X_F, len('--') * MULT),
]
"""Overlaps between fragments, fisrt connected component.

- u_f -> v_f
- v_f -> w_r
- w_r -> x_f

```txt
-----> u
   ----------> v
           <-------- w
                  --------> x
```
"""

#
# Two connected components
#
YZ_IND = 3

OVERLAPS_CC1_CC2: list[OverlapTestT] = OVERLAPS_CC1 + [
    (*Y_F, *Z_R, len('---->') * MULT),
]
"""Overlaps between fragments, second connected component.

- y_f -> z_r

```txt
-------> y
   <------- z
```
"""

OVERLAPS_INVCMP_CC1: list[OverlapTestT] = OVERLAPS_CC1 + [
    (*X_F, *V_R, len('-->') * MULT),
]
"""Overlaps plus one that connects forwards with their reverse.

- x_f -> v_r

```txt
--------> x
      <---------- v
```
"""

#
# Transitive
#
UA_IND = 3
AV_IND = 4

OVERLAPS_TRANS_CC1: list[OverlapTestT] = OVERLAPS_CC1 + [
    (*U_F, *A_F, len('---->') * MULT),
    (*A_F, *V_F, len('---->') * MULT),
    (*V_F, *B_F, len('--->') * MULT),
    (*B_F, *W_R, len('----->') * MULT),
]
"""Overlaps that transitively connect CC1.

- u_f -> a_f
- a_f -> v_f
- v_f -> b_f
- b_f -> w_r

```txt
-----> u
 ------> a
   ----------> v
          ------> b
           <-------- w
                  --------> x
```
"""

# ---------------------------------------------------------------------------- #
#                             Nodes And Edges Pull                             #
# ---------------------------------------------------------------------------- #
SUB_NODES_CC1 = (V_R, U_R, W_F, X_R)
SUB_EDGES_CC1 = (
    (V_R, U_R),
    (W_F, V_R),
    (X_R, W_F),
)


# ============================================================================ #
#                                GRAPH FUNCTION                                #
# ============================================================================ #
def add_overlap_to_graph(graph: RevSymGraph, overlap: OverlapTestT):
    """Add overlap to the graph.

    Parameters
    ----------
    graph : RevSymGraph
        Reverse symmetric graph
    overlap : OverlapTestT
        Overlap information
    """
    u_ind, u_or, v_ind, v_or, uv_len = overlap

    if u_ind not in graph.vertices():
        assert graph.vertices().add() == u_ind
    if v_ind not in graph.vertices():
        assert graph.vertices().add() == v_ind

    e_ind = graph.edges().add((u_ind, u_or), (v_ind, v_or))
    graph.edges().set_attr(e_ind, ATTR_OVLEN, uv_len)
