# -*- coding=utf-8 -*-

"""Tests for hashable reads' identifiers container."""

# pylint: disable=redefined-outer-name, compare-to-zero,
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc

from typing import Iterator

import pytest

from revsymg.exceptions import NoVertexIndex
from revsymg.id_containers import IndexIDContainer, NoReadId
from revsymg.index_lib import FORWARD_INT, IND


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def ind_rids() -> Iterator[IndexIDContainer]:
    """Yield an empty hashable read's identifiers container."""
    # Arrange
    ind_rids = IndexIDContainer()
    yield ind_rids
    # Clean-up


# ============================================================================ #
#                                TESTS FUNCTIONS                               #
# ============================================================================ #
def test_id_to_indor(ind_rids: IndexIDContainer):
    """Test id_to_indor method."""
    rid = 0
    try:
        _ = ind_rids.id_to_indor(rid)
        raise AssertionError
    except NoReadId:
        assert True
    index = ind_rids.add(rid)
    assert ind_rids.id_to_indor(rid, FORWARD_INT) == (index, FORWARD_INT)


def test_indor_to_id(ind_rids: IndexIDContainer):
    """Test indor_to_id method."""
    rid = 0
    indor = (0, FORWARD_INT)
    try:
        _ = ind_rids.indor_to_id(indor)
        raise AssertionError
    except NoVertexIndex:
        assert True
    index = ind_rids.add(rid)
    assert index == indor[IND]
    assert ind_rids.indor_to_id(indor) == rid


def test_add(ind_rids: IndexIDContainer):
    """Test add method."""
    rid = 0
    # First time added
    assert ind_rids.add(rid) == 0
    # already added
    assert ind_rids.add(rid) == 0
    rid_2 = 1
    # First time added
    assert ind_rids.add(rid_2) == 1
    # already added
    assert ind_rids.add(rid_2) == 1


def test_pop(ind_rids: IndexIDContainer):
    """Test pop method."""
    rid = 0
    rid_2 = 1
    try:
        _ = ind_rids.pop(rid)
        raise AssertionError
    except NoReadId:
        assert True
    index = ind_rids.add(rid)
    index_2 = ind_rids.add(rid_2)
    assert index == ind_rids.pop(rid) == 0
    assert ind_rids.id_to_indor(rid_2)[IND] == index_2 - 1 == 0
    assert ind_rids.indor_to_id((index_2 - 1, FORWARD_INT)) == rid_2


def test_iter(ind_rids: IndexIDContainer):
    """Test __iter__ method."""
    assert not list(ind_rids)
    rids = [0, 1]
    for rid in rids:
        ind_rids.add(rid)
    assert set(ind_rids) == set(rids)


def test_contains(ind_rids: IndexIDContainer):
    """Test __contains__ method."""
    rid = 0
    assert rid not in ind_rids
    ind_rids.add(rid)
    assert rid in ind_rids


def test_len(ind_rids: IndexIDContainer):
    """Test __len__ method."""
    rid = 0
    assert len(ind_rids) == 0
    ind_rids.add(rid)
    assert len(ind_rids) == 1
