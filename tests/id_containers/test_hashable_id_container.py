# -*- coding=utf-8 -*-

"""Tests for hashable reads' identifiers container."""

# pylint: disable=redefined-outer-name, compare-to-zero,
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc

from typing import Iterator

import pytest

from revsymg.exceptions import NoVertexIndex
from revsymg.id_containers import HashableIDContainer, NoReadId
from revsymg.index_lib import FORWARD_INT, IND


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def hash_rids() -> Iterator[HashableIDContainer]:
    """Yield an empty hashable read's identifiers container."""
    # Arrange
    hash_rids = HashableIDContainer()
    yield hash_rids
    # Clean-up


# ============================================================================ #
#                                TESTS FUNCTIONS                               #
# ============================================================================ #
def test_id_to_indor(hash_rids: HashableIDContainer):
    """Test id_to_indor method."""
    rid = 'read_id'
    try:
        _ = hash_rids.id_to_indor(rid)
        raise AssertionError
    except NoReadId:
        assert True
    index = hash_rids.add(rid)
    assert hash_rids.id_to_indor(rid, FORWARD_INT) == (index, FORWARD_INT)


def test_indor_to_id(hash_rids: HashableIDContainer):
    """Test indor_to_id method."""
    rid = 'read_id'
    indor = (0, FORWARD_INT)
    try:
        _ = hash_rids.indor_to_id(indor)
        raise AssertionError
    except NoVertexIndex:
        assert True
    index = hash_rids.add(rid)
    assert index == indor[IND]
    assert hash_rids.indor_to_id(indor) == rid


def test_add(hash_rids: HashableIDContainer):
    """Test add method."""
    rid = 'reads_id'
    # First time added
    assert hash_rids.add(rid) == 0
    # already added
    assert hash_rids.add(rid) == 0
    rid_2 = 'reads_id_2'
    # First time added
    assert hash_rids.add(rid_2) == 1
    # already added
    assert hash_rids.add(rid_2) == 1


def test_pop(hash_rids: HashableIDContainer):
    """Test pop method."""
    rid = 'reads_id'
    rid_2 = 'reads_id_2'
    try:
        _ = hash_rids.pop(rid)
        raise AssertionError
    except NoReadId:
        assert True
    index = hash_rids.add(rid)
    index_2 = hash_rids.add(rid_2)
    assert index == hash_rids.pop(rid) == 0
    assert hash_rids.id_to_indor(rid_2)[IND] == index_2 - 1 == 0


def test_iter(hash_rids: HashableIDContainer):
    """Test __iter__ method."""
    assert not list(hash_rids)
    rids = ['reads_id', 'reads_id_2']
    for rid in rids:
        hash_rids.add(rid)
    assert set(hash_rids) == set(rids)


def test_contains(hash_rids: HashableIDContainer):
    """Test __contains__ method."""
    rid = 'reads_id'
    assert rid not in hash_rids
    hash_rids.add(rid)
    assert rid in hash_rids


def test_len(hash_rids: HashableIDContainer):
    """Test __len__ method."""
    rid = 'reads_id'
    assert len(hash_rids) == 0
    hash_rids.add(rid)
    assert len(hash_rids) == 1
