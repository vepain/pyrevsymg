# -*- coding=utf-8 -*-

"""Tests for abstract ID containers."""

from revsymg.id_containers import NoReadId


def test_noreadid():
    """Test NoReadId exc."""
    assert (
        str(NoReadId(0))
        == "There is no read's identifier `0` in container."
    )
    assert (
        str(NoReadId('id'))
        == "There is no read's identifier `id` in container."
    )
