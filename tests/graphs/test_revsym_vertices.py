# -*- coding=utf-8 -*-

"""Tests for vertices container."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc


from typing import Iterator

import pytest

from revsymg.exceptions import (
    NoVertexIndex,
    NoVerticesAttribute,
    WrongAttributeType,
)
from revsymg.graphs import AttributesContainer
from revsymg.graphs._adjancy_table import _AdjsT
from revsymg.graphs.revsym_vertices import Vertices
from revsymg.index_lib import IND, OR, PRED_IND, SUCC_IND
from tests.datatest import (
    U_F,
    U_IND,
    U_R,
    UV_IND,
    V_F,
    V_IND,
    V_R,
    VW_IND,
    W_IND,
    W_R,
)
from tests.graphs.test_attributes import (
    ATTR_DEF,
    ATTR_NAME,
    ATTR_VAL,
    ATTRS,
    NO_ATTR_NAME,
    NO_ATTR_TYPE,
)


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
NO_INDEX = 10


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def revsym_vertices(revsym_adj_tab: _AdjsT) -> Iterator[Vertices]:
    """Yield an empty vertices container."""
    # Arrange
    _revsym_vertices = Vertices(revsym_adj_tab)
    yield _revsym_vertices
    # Clean-up


@pytest.fixture
def revsym_vertices_attr(revsym_adj_tab_u_v: _AdjsT,
                         attr_container_2: AttributesContainer) -> (
        Iterator[Vertices]):
    """Yield a vertices container with attribute."""
    # Arrange
    _revsym_vertices_attr = Vertices(revsym_adj_tab_u_v)
    _revsym_vertices_attr._attributes = attr_container_2
    _revsym_vertices_attr._card_vf = 2
    yield _revsym_vertices_attr
    # Clean-up


@pytest.fixture
def revsym_vertices_uf_vf(revsym_adj_tab_uf_vf: _AdjsT,
                          attr_container_2: AttributesContainer) -> (
        Iterator[Vertices]):
    """Yield a vertices container with edges."""
    # Arrange
    _revsym_vertices_uf_vf = Vertices(revsym_adj_tab_uf_vf)
    _revsym_vertices_uf_vf._attributes = attr_container_2
    _revsym_vertices_uf_vf._card_vf = 2
    yield _revsym_vertices_uf_vf
    # Clean-up


@pytest.fixture
def revsym_vertices_uf_vf_wr(revsym_adj_tab_uf_vf_wr: _AdjsT,
                             attr_container_3: AttributesContainer) -> (
        Iterator[Vertices]):
    """Yield a vertices container with edges."""
    # Arrange
    _revsym_vertices_uf_vf_wr = Vertices(revsym_adj_tab_uf_vf_wr)
    _revsym_vertices_uf_vf_wr._attributes = attr_container_3
    _revsym_vertices_uf_vf_wr._card_vf = 3
    yield _revsym_vertices_uf_vf_wr
    # Clean-up


@pytest.fixture
def revsym_vertices_uf_vf_wr_xf(revsym_adj_tab_uf_vf_wr_xf: _AdjsT,
                                attr_container_4: AttributesContainer) -> (
        Iterator[Vertices]):
    """Yield a vertices container with all edge types."""
    # Arrange
    _revsym_vertices_uf_vf_wr_xf = Vertices(revsym_adj_tab_uf_vf_wr_xf)
    _revsym_vertices_uf_vf_wr_xf._attributes = attr_container_4
    _revsym_vertices_uf_vf_wr_xf._card_vf = 4
    yield _revsym_vertices_uf_vf_wr_xf
    # Clean-up


# ============================================================================ #
#                                TESTS FUNCTIONS                               #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                  Attr Method                                 #
# ---------------------------------------------------------------------------- #
def test_attr_no_vertex_index(revsym_vertices_attr: Vertices):
    """Test attr method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_vertices_attr.attr(NO_INDEX, ATTR_NAME)


def test_attr_no_vertices_attribute(revsym_vertices_attr: Vertices):
    """Test attr method for NoVerticesAttribute exc."""
    with pytest.raises(NoVerticesAttribute):
        revsym_vertices_attr.attr(U_IND, NO_ATTR_NAME)


def test_attr_ok(revsym_vertices_attr: Vertices):
    """Test attr method with success wanted."""
    assert revsym_vertices_attr.attr(U_IND, ATTR_NAME) == ATTR_DEF
    assert revsym_vertices_attr.attr(V_IND, ATTR_NAME) == ATTR_VAL


# ---------------------------------------------------------------------------- #
#                                 Attrs Method                                 #
# ---------------------------------------------------------------------------- #
def test_attrs_no_vertex_index(revsym_vertices: Vertices):
    """Test attrs method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_vertices.attrs(U_IND):
            pass


def test_attrs_ok(revsym_vertices_attr: Vertices):
    """Test attrs method with success wanted."""
    assert dict(revsym_vertices_attr.attrs(V_IND)) == ATTRS


# ---------------------------------------------------------------------------- #
#                                   New_attr                                   #
# ---------------------------------------------------------------------------- #
def test_new_attr(revsym_vertices: Vertices):
    """Test new_attr meth."""
    revsym_vertices.new_attr(ATTR_NAME, ATTR_DEF)


# ---------------------------------------------------------------------------- #
#                               Set_attr Method                               #
# ---------------------------------------------------------------------------- #
def test_set_attr_ok(revsym_vertices_attr: Vertices):
    """Test set_attr method."""
    revsym_vertices_attr.set_attr(U_IND, ATTR_NAME, ATTR_VAL)


def test_set_attr_no_vertices_attr(revsym_vertices_attr: Vertices):
    """Test set_attr method for NoverticesAttribute exc."""
    with pytest.raises(NoVerticesAttribute):
        for _ in revsym_vertices_attr.set_attr(U_IND, NO_ATTR_NAME, ATTR_VAL):
            pass


def test_set_attr_wrong_type_attr(revsym_vertices_attr: Vertices):
    """Test set_attr method for WrongAttributeType exc."""
    with pytest.raises(WrongAttributeType):
        for _ in revsym_vertices_attr.set_attr(U_IND, ATTR_NAME, NO_ATTR_TYPE):
            pass


def test_set_attr_no_vertex_index(revsym_vertices_attr: Vertices):
    """Test set_attr method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_vertices_attr.set_attr(NO_INDEX, ATTR_NAME, ATTR_VAL):
            pass


# ---------------------------------------------------------------------------- #
#                                  Card_index                                  #
# ---------------------------------------------------------------------------- #
def test_card_index_empty(revsym_vertices: Vertices):
    """Test card_index meth when empty."""
    assert revsym_vertices.card_index() == 0


def test_card_index_uf_vf(revsym_vertices_uf_vf: Vertices):
    """Test card_index meth when uf vf."""
    assert revsym_vertices_uf_vf.card_index() == 2


# ---------------------------------------------------------------------------- #
#                                  Add Method                                  #
# ---------------------------------------------------------------------------- #
def test_add_one_by_one(revsym_vertices: Vertices):
    """Test add method."""
    assert revsym_vertices.add() == 0
    assert revsym_vertices.add() == 1
    assert revsym_vertices.add() == 2
    assert (
        len(revsym_vertices._adjs[PRED_IND])
        == len(revsym_vertices._adjs[SUCC_IND])
        == revsym_vertices._card_vf
        == 3
    )


def test_add_several(revsym_vertices: Vertices):
    """Test add method."""
    assert revsym_vertices.add(3) == 2
    assert (
        len(revsym_vertices._adjs[PRED_IND])
        == len(revsym_vertices._adjs[SUCC_IND])
        == revsym_vertices._card_vf
        == 3
    )


# ---------------------------------------------------------------------------- #
#                                 Delete Method                                #
# ---------------------------------------------------------------------------- #
def test_delete_no_vertex_index(revsym_vertices: Vertices):
    """Test delete method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_vertices.delete(U_IND)


def test_delete_vertex_uf_vf_wr_u(revsym_vertices_uf_vf_wr: Vertices):
    """Test delete method on u with edges."""
    revsym_vertices_uf_vf_wr.delete(U_IND)
    assert (
        revsym_vertices_uf_vf_wr._adjs[PRED_IND]
        == [
            [],
            [],
        ]
    )
    assert (
        revsym_vertices_uf_vf_wr._adjs[SUCC_IND]
        == [
            [
                ((W_R[IND] - 1, W_R[OR]), VW_IND),
            ],
            [
                ((V_R[IND] - 1, V_R[OR]), VW_IND),
            ],
        ]
    )
    assert revsym_vertices_uf_vf_wr._card_vf == 2


def test_delete_vertex_uf_vf_wr_v(revsym_vertices_uf_vf_wr: Vertices):
    """Test delete method on v with edges."""
    revsym_vertices_uf_vf_wr.delete(V_IND)
    assert (
        revsym_vertices_uf_vf_wr._adjs[PRED_IND]
        == [
            [],
            [],
        ]
    )
    assert (
        revsym_vertices_uf_vf_wr._adjs[SUCC_IND]
        == [
            [],
            [],
        ]
    )
    assert revsym_vertices_uf_vf_wr._card_vf == 2


def test_delete_vertex_uf_vf_wr_w(revsym_vertices_uf_vf_wr: Vertices):
    """Test delete method on w with edges."""
    revsym_vertices_uf_vf_wr.delete(W_IND)
    assert (
        revsym_vertices_uf_vf_wr._adjs[PRED_IND]
        == [
            [],
            [
                (U_F, UV_IND),
            ],
        ]
    )
    assert (
        revsym_vertices_uf_vf_wr._adjs[SUCC_IND]
        == [
            [
                (V_F, UV_IND),
            ],
            [],
        ]
    )
    assert revsym_vertices_uf_vf_wr._card_vf == 2


# ---------------------------------------------------------------------------- #
#                                Delete_several                                #
# ---------------------------------------------------------------------------- #
def test_delete_several_no_vertex_index(revsym_vertices_uf_vf: Vertices):
    """Test delete method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_vertices_uf_vf.delete_several((U_IND, W_IND, V_IND))


def test_delete_several_uf_vf_wr_u_w(revsym_vertices_uf_vf_wr: Vertices):
    """Test delete_several ok."""
    revsym_vertices_uf_vf_wr.delete_several((U_IND, W_IND))
    assert (
        revsym_vertices_uf_vf_wr._adjs[PRED_IND]
        == [
            [],
        ]
    )
    assert (
        revsym_vertices_uf_vf_wr._adjs[SUCC_IND]
        == [
            [],
        ]
    )
    assert revsym_vertices_uf_vf_wr._card_vf == 1


# ---------------------------------------------------------------------------- #
#                                __iter__ Method                               #
# ---------------------------------------------------------------------------- #
def test_iter_empty(revsym_vertices: Vertices):
    """Test iter method when empty."""
    assert not tuple(v for v in revsym_vertices)


def test_iter_uf_vf(revsym_vertices_uf_vf: Vertices):
    """Test iter method when u is deleted."""
    assert tuple(v for v in revsym_vertices_uf_vf) == (U_F, U_R, V_F, V_R)


# ---------------------------------------------------------------------------- #
#                              __contains__ Method                             #
# ---------------------------------------------------------------------------- #
def test_contains_empty(revsym_vertices: Vertices):
    """Test contains method when empty."""
    assert U_IND not in revsym_vertices


def test_contains_uf_vf(revsym_vertices_uf_vf: Vertices):
    """Test contains method when uf vf."""
    assert U_IND in revsym_vertices_uf_vf
    assert V_IND in revsym_vertices_uf_vf


# ---------------------------------------------------------------------------- #
#                                __len__ Method                                #
# ---------------------------------------------------------------------------- #
def test_len_empty(revsym_vertices: Vertices):
    """Test len meth when empty."""
    assert len(revsym_vertices) == 0


def test_len_uf_vf(revsym_vertices_uf_vf: Vertices):
    """Test len meth when uf vf."""
    assert len(revsym_vertices_uf_vf) == 4
