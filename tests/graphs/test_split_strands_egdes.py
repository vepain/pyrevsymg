# -*- coding=utf-8 -*-

"""Tests for edges container."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc


from typing import Iterator

import pytest
from bitarray import bitarray

from revsymg.exceptions import (
    NoEdgeIndex,
    NoEdgesAttribute,
    NoIndicesEdge,
    NoVertexIndex,
    WrongAttributeType,
)
from revsymg.graphs import AttributesContainer
from revsymg.graphs._adjancy_table import _AdjsT
from revsymg.graphs.split_strands_edges import SplitStrandsEdges
from tests.datatest import (
    A_IND,
    AV_IND,
    U_IND,
    UA_IND,
    UV_IND,
    V_IND,
    VW_IND,
    W_IND,
    WX_IND,
    X_IND,
)
from tests.graphs.test_attributes import (
    ATTR_DEF,
    ATTR_NAME,
    ATTR_VAL,
    ATTRS,
    NO_ATTR_NAME,
    NO_ATTR_TYPE,
)


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
NO_INDEX = 10


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def split_strands_edges(indices_adj_tab: _AdjsT) -> Iterator[SplitStrandsEdges]:
    """Yield an empty edges container."""
    # Arrange
    _split_strands_edges = SplitStrandsEdges(indices_adj_tab, bitarray())
    yield _split_strands_edges
    # Clean-up


@pytest.fixture
def split_strands_edges_u_v(indices_adj_tab_u_v: _AdjsT) -> (
        Iterator[SplitStrandsEdges]):
    """Yield a edges container with two vertices and no edge."""
    # Arrange
    _split_strands_edges_u_v = SplitStrandsEdges(
        indices_adj_tab_u_v, bitarray('00'),
    )
    yield _split_strands_edges_u_v
    # Clean-up


@pytest.fixture
def split_strands_edges_attr(indices_adj_tab_uf_vf: _AdjsT,
                             attr_container_1: AttributesContainer) -> (
        Iterator[SplitStrandsEdges]):
    """Yield a edges container with one edge and one attribute."""
    # Arrange
    _split_strands_edges_attr = SplitStrandsEdges(
        indices_adj_tab_uf_vf, bitarray('00'),
    )
    _split_strands_edges_attr._attributes = attr_container_1
    _split_strands_edges_attr._card_e = 1
    _split_strands_edges_attr._ind_e = 0
    yield _split_strands_edges_attr
    # Clean-up


@pytest.fixture
def split_strands_edges_uf_vf_wr(indices_adj_tab_uf_vf_wr: _AdjsT,
                                 attr_container_2: AttributesContainer) -> (
        Iterator[SplitStrandsEdges]):
    """Yield a edges container with edges."""
    # Arrange
    _split_strands_edges_uf_vf_wr = SplitStrandsEdges(
        indices_adj_tab_uf_vf_wr, bitarray('001'),
    )
    _split_strands_edges_uf_vf_wr._attributes = attr_container_2
    _split_strands_edges_uf_vf_wr._card_e = 2
    _split_strands_edges_uf_vf_wr._ind_e = 1
    yield _split_strands_edges_uf_vf_wr
    # Clean-up


@pytest.fixture
def split_strands_edges_uf_vf_wr_xf(indices_adj_tab_uf_vf_wr_xf: _AdjsT,
                                    attr_container_3: AttributesContainer) -> (
        Iterator[SplitStrandsEdges]):
    """Yield a edges container with all edge types."""
    # Arrange
    _split_strands_edges_uf_vf_wr_xf = SplitStrandsEdges(
        indices_adj_tab_uf_vf_wr_xf, bitarray('0010'),
    )
    _split_strands_edges_uf_vf_wr_xf._adjs = indices_adj_tab_uf_vf_wr_xf
    _split_strands_edges_uf_vf_wr_xf._attributes = attr_container_3
    _split_strands_edges_uf_vf_wr_xf._card_e = 3
    _split_strands_edges_uf_vf_wr_xf._ind_e = 2
    yield _split_strands_edges_uf_vf_wr_xf
    # Clean-up


@pytest.fixture
def split_strands_edges_uf_af_vf(indices_adj_tab_uf_af_vf: _AdjsT,
                                 attr_container_2: AttributesContainer) -> (
        Iterator[SplitStrandsEdges]):
    """Yield a edges container with transitive edges."""
    # Arrange
    _split_strands_edges_uf_af_vf = SplitStrandsEdges(
        indices_adj_tab_uf_af_vf, bitarray('000'),
    )
    _split_strands_edges_uf_af_vf._attributes = attr_container_2
    _split_strands_edges_uf_af_vf._card_e = 2
    _split_strands_edges_uf_af_vf._ind_e = 1
    yield _split_strands_edges_uf_af_vf
    # Clean-up


# ============================================================================ #
#                                TESTS FUNCTIONS                               #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                     Attr                                     #
# ---------------------------------------------------------------------------- #
def test_attr_no_edges_attribute(split_strands_edges_attr: SplitStrandsEdges):
    """Test attr method for NoSplitStrandsEdgesAttribute exc."""
    with pytest.raises(NoEdgesAttribute):
        split_strands_edges_attr.attr(UV_IND, NO_ATTR_NAME)


def test_attr_no_edge_index(split_strands_edges_attr: SplitStrandsEdges):
    """Test attr method for NoEdgeAttribute exc."""
    with pytest.raises(NoEdgeIndex):
        split_strands_edges_attr.attr(NO_INDEX, ATTR_NAME)


def test_attr_ok(split_strands_edges_attr: SplitStrandsEdges):
    """Test attr method with success wanted."""
    assert split_strands_edges_attr.attr(UV_IND, ATTR_NAME) == ATTR_VAL


# ---------------------------------------------------------------------------- #
#                                     Attrs                                    #
# ---------------------------------------------------------------------------- #
def test_attrs_no_edge_index(split_strands_edges: SplitStrandsEdges):
    """Test attrs method for NoEdgeIndex exc."""
    with pytest.raises(NoEdgeIndex):
        assert not dict(split_strands_edges.attrs(UV_IND))


def test_attrs_ok(split_strands_edges_attr: SplitStrandsEdges):
    """Test attrs method with success wanted."""
    assert dict(split_strands_edges_attr.attrs(UV_IND)) == ATTRS


# ---------------------------------------------------------------------------- #
#                                   New_attr                                   #
# ---------------------------------------------------------------------------- #
def test_new_attr(split_strands_edges: SplitStrandsEdges):
    """Test new_attr meth."""
    split_strands_edges.new_attr(ATTR_NAME, ATTR_DEF)


# ---------------------------------------------------------------------------- #
#                                   Set_attr                                   #
# ---------------------------------------------------------------------------- #
def test_set_attr_ok(split_strands_edges_attr: SplitStrandsEdges):
    """Test set_attr method."""
    split_strands_edges_attr.set_attr(UV_IND, ATTR_NAME, ATTR_VAL)


def test_set_attr_no_vertices_attr(split_strands_edges_attr: SplitStrandsEdges):
    """Test set_attr method for NoverticesAttribute exc."""
    with pytest.raises(NoEdgesAttribute):
        for _ in split_strands_edges_attr.set_attr(
                UV_IND, NO_ATTR_NAME, ATTR_VAL):
            pass


def test_set_attr_wrong_type_attr(split_strands_edges_attr: SplitStrandsEdges):
    """Test set_attr method for WrongAttributeType exc."""
    with pytest.raises(WrongAttributeType):
        for _ in split_strands_edges_attr.set_attr(
                UV_IND, ATTR_NAME, NO_ATTR_TYPE):
            pass


def test_set_attr_no_vertex_index(split_strands_edges_attr: SplitStrandsEdges):
    """Test set_attr method for NoVertexIndex exc."""
    with pytest.raises(NoEdgeIndex):
        for _ in split_strands_edges_attr.set_attr(
                NO_INDEX, ATTR_NAME, ATTR_VAL):
            pass


# ---------------------------------------------------------------------------- #
#                                Eindor_to_eind                                #
# ---------------------------------------------------------------------------- #
def test_eindor_to_eind_novertexindex_1(split_strands_edges: SplitStrandsEdges):
    """Test eindor_to_eind meth for u NoVertexIndex esc."""
    with pytest.raises(NoVertexIndex):
        for _ in split_strands_edges.eindor_to_eind(U_IND, V_IND):
            pass


def test_eindor_to_eind_novertexindex_2(
        split_strands_edges_attr: SplitStrandsEdges):
    """Test eindor_to_eind meth for v NoVertexIndex esc."""
    with pytest.raises(NoVertexIndex):
        for _ in split_strands_edges_attr.eindor_to_eind(U_IND, W_IND):
            pass


def test_eindor_to_eind_ok(split_strands_edges_uf_vf_wr: SplitStrandsEdges):
    """Test eindor_to_eind meth ok."""
    assert (
        tuple(split_strands_edges_uf_vf_wr.eindor_to_eind(U_IND, V_IND))
        == (UV_IND, )
    )
    assert not tuple(
        split_strands_edges_uf_vf_wr.eindor_to_eind(V_IND, U_IND),
    )


# ---------------------------------------------------------------------------- #
#                                     Preds                                    #
# ---------------------------------------------------------------------------- #
def test_preds_novertexindex(split_strands_edges: SplitStrandsEdges):
    """Test preds meth for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in split_strands_edges.preds(U_IND):
            pass


def test_preds_vf(split_strands_edges_uf_vf_wr: SplitStrandsEdges):
    """Test preds meth for vf when two edges."""
    assert (
        tuple(split_strands_edges_uf_vf_wr.preds(V_IND))
        == ((U_IND, UV_IND),)
    )


# ---------------------------------------------------------------------------- #
#                                     Succs                                    #
# ---------------------------------------------------------------------------- #
def test_succs_novertexindex(split_strands_edges: SplitStrandsEdges):
    """Test succs meth for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in split_strands_edges.succs(U_IND):
            pass


def test_succs_vf(split_strands_edges_uf_vf_wr: SplitStrandsEdges):
    """Test succs meth for vf when two edges."""
    assert (
        tuple(split_strands_edges_uf_vf_wr.succs(V_IND))
        == ((W_IND, VW_IND),)
    )


# ---------------------------------------------------------------------------- #
#                                  Neighbours                                  #
# ---------------------------------------------------------------------------- #
def test_neighbours_novertexindex(split_strands_edges: SplitStrandsEdges):
    """Test neighbours meth for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in split_strands_edges.neighbours(U_IND):
            pass


def test_neighbours_vf(split_strands_edges_uf_vf_wr: SplitStrandsEdges):
    """Test neighbours meth for vf when two edges."""
    assert (
        tuple(split_strands_edges_uf_vf_wr.neighbours(V_IND))
        == ((U_IND, UV_IND), (W_IND, VW_IND))
    )


# ---------------------------------------------------------------------------- #
#                                      Add                                     #
# ---------------------------------------------------------------------------- #
def test_add_novertexindex_1(split_strands_edges_attr: SplitStrandsEdges):
    """Test add meth for first NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        split_strands_edges_attr.add(W_IND, V_IND)


def test_add_novertexindex_2(split_strands_edges_attr: SplitStrandsEdges):
    """Test add meth for second NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        split_strands_edges_attr.add(V_IND, W_IND)


def test_add_uf_vf(split_strands_edges_u_v: SplitStrandsEdges):
    """Test add meth for uf vf edge."""
    assert split_strands_edges_u_v.add(U_IND, V_IND) == UV_IND
    assert split_strands_edges_u_v._card_e == 1
    assert split_strands_edges_u_v._ind_e == 0
    assert split_strands_edges_u_v._adjs == (
        [
            [],
            [(U_IND, UV_IND)],
        ],
        [
            [(V_IND, UV_IND)],
            [],
        ],
    )
    assert split_strands_edges_u_v._attributes._card_keys == 1


# ---------------------------------------------------------------------------- #
#                                    Delete                                    #
# ---------------------------------------------------------------------------- #
def test_delete_novertexindex_1(split_strands_edges_u_v: SplitStrandsEdges):
    """Test delete meth for first NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        split_strands_edges_u_v.delete(W_IND, V_IND, VW_IND)


def test_delete_novertexindex_2(split_strands_edges_u_v: SplitStrandsEdges):
    """Test delete meth for second NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        split_strands_edges_u_v.delete(V_IND, W_IND, VW_IND)


def test_delete_noedge(split_strands_edges_uf_af_vf: SplitStrandsEdges):
    """Test delete meth for first NoEdge exc."""
    with pytest.raises(NoIndicesEdge):
        split_strands_edges_uf_af_vf.delete(U_IND, V_IND, NO_INDEX)


def test_delete_uf_af_vf_ua(split_strands_edges_uf_af_vf: SplitStrandsEdges):
    """Test delete meth on uf af edge."""
    split_strands_edges_uf_af_vf.delete(U_IND, A_IND, UA_IND)
    assert split_strands_edges_uf_af_vf._adjs == (
        [
            [],
            [(U_IND, UV_IND), (A_IND, AV_IND)],
            [], [], [],
        ],
        [
            [(V_IND, UV_IND)],
            [], [], [],
            [(V_IND, AV_IND)],
        ],
    )


def test_delete_uf_af_vf_uv(split_strands_edges_uf_af_vf: SplitStrandsEdges):
    """Test delete meth on uf vf edge."""
    split_strands_edges_uf_af_vf.delete(U_IND, V_IND, UV_IND)
    assert split_strands_edges_uf_af_vf._adjs == (
        [
            [],
            [(A_IND, AV_IND)],
            [], [],
            [(U_IND, UA_IND)],
        ],
        [
            [(A_IND, UA_IND)],
            [], [], [],
            [(V_IND, AV_IND)],
        ],
    )


# ---------------------------------------------------------------------------- #
#                                __iter__ Method                               #
# ---------------------------------------------------------------------------- #
def test_iter_empty(split_strands_edges: SplitStrandsEdges):
    """Test iter method when empty."""
    assert not tuple(v for v in split_strands_edges)


def test_iter_uf_vf_wr_xf(split_strands_edges_uf_vf_wr_xf: SplitStrandsEdges):
    """Test iter method with all edge types."""
    assert tuple(v for v in split_strands_edges_uf_vf_wr_xf) == (
        (U_IND, V_IND, UV_IND),
        (V_IND, W_IND, VW_IND),
        (W_IND, X_IND, WX_IND),
    )


# ---------------------------------------------------------------------------- #
#                              __contains__ Method                             #
# ---------------------------------------------------------------------------- #
def test_contains_empty(split_strands_edges: SplitStrandsEdges):
    """Test contains method when empty."""
    assert (U_IND, V_IND) not in split_strands_edges


def test_contains_false_2(split_strands_edges_u_v: SplitStrandsEdges):
    """Test contains method when empty."""
    assert (V_IND, W_IND) not in split_strands_edges_u_v


def test_contains_uf_vf_forward(
        split_strands_edges_uf_vf_wr: SplitStrandsEdges):
    """Test contains method when uf vf forward if."""
    assert (U_IND, V_IND) in split_strands_edges_uf_vf_wr


# ---------------------------------------------------------------------------- #
#                                __len__ Method                                #
# ---------------------------------------------------------------------------- #
def test_len_empty(split_strands_edges: SplitStrandsEdges):
    """Test len meth when empty."""
    assert len(split_strands_edges) == 0


def test_len_uf_vf_wr(split_strands_edges_uf_vf_wr: SplitStrandsEdges):
    """Test len meth when uf vf."""
    assert len(split_strands_edges_uf_vf_wr) == 2
