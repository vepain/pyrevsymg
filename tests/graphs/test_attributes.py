# -*- coding=utf-8 -*-

"""Tests for attributes container."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc

from typing import Iterator

import pytest

from revsymg.exceptions import WrongAttributeType, _NoAttribute, _NoKey
from revsymg.graphs import AttributesContainer


# TODO: Separate tests

# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
__KEY_0 = 0
__KEY_1 = 1
__NO_KEY = 10

ATTR_NAME = 'attr_name'
NO_ATTR_NAME = 'no_attr_name'
ATTR_VAL = 'attr_val'
NO_ATTR_TYPE = 42
__ATTR_IND = 0
ATTR_DEF = ''
ATTRS = {ATTR_NAME: ATTR_VAL}


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def attr_container() -> Iterator[AttributesContainer]:
    """Yield an empty attribute container."""
    # Arrange
    attr_container = AttributesContainer()
    yield attr_container
    # Clean-up


@pytest.fixture
def attr_container_1(attr_container: AttributesContainer) -> (
        Iterator[AttributesContainer]):
    """Yield an attribute container where first key has one attr."""
    # Arrange
    attr_container._card_keys = 1
    attr_container._attrname_index_default = {
        ATTR_NAME: (__ATTR_IND, ATTR_DEF),
    }
    attr_container._lists_values = [
        [ATTR_VAL],
    ]
    yield attr_container


@pytest.fixture
def attr_container_2(attr_container: AttributesContainer) -> (
        Iterator[AttributesContainer]):
    """Yield an attribute container where second key has no default attr."""
    # Arrange
    attr_container._card_keys = 2
    attr_container._attrname_index_default = {
        ATTR_NAME: (__ATTR_IND, ATTR_DEF),
    }
    attr_container._lists_values = [
        [ATTR_DEF, ATTR_VAL],
    ]
    yield attr_container


@pytest.fixture
def attr_container_3(attr_container: AttributesContainer) -> (
        Iterator[AttributesContainer]):
    """Yield an attribute container where second key has no default attr."""
    # Arrange
    attr_container._card_keys = 3
    attr_container._attrname_index_default = {
        ATTR_NAME: (__ATTR_IND, ATTR_DEF),
    }
    attr_container._lists_values = [
        [ATTR_DEF, ATTR_VAL, ATTR_DEF],
    ]
    yield attr_container


@pytest.fixture
def attr_container_4() -> (
        Iterator[AttributesContainer]):
    """Yield an attribute container where second key has no default attr."""
    # Arrange
    _attr_container_4 = AttributesContainer()
    _attr_container_4._card_keys = 4
    _attr_container_4._attrname_index_default = {
        ATTR_NAME: (__ATTR_IND, ATTR_DEF),
    }
    _attr_container_4._lists_values = [
        [ATTR_DEF, ATTR_VAL, ATTR_DEF, ATTR_DEF],
    ]
    yield _attr_container_4


# ============================================================================ #
#                                TESTS FUNCTIONS                               #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                Card_properties                               #
# ---------------------------------------------------------------------------- #
def test_card_properties(attr_container: AttributesContainer):
    """Test card_properties method."""
    assert attr_container.card_properties() == 0


def test_card_properties_one(attr_container_1: AttributesContainer):
    """Test card method when one attr."""
    assert attr_container_1.card_properties() == 1


# ---------------------------------------------------------------------------- #
#                                 Names Method                                 #
# ---------------------------------------------------------------------------- #
def test_names(attr_container: AttributesContainer):
    """Test names method."""
    assert not tuple(attr_container.names())


def test_names_one(attr_container_1: AttributesContainer):
    """Test names method when one attr."""
    assert tuple(attr_container_1.names()) == (ATTR_NAME,)


# ---------------------------------------------------------------------------- #
#                                 Has_attrname                                 #
# ---------------------------------------------------------------------------- #
def test_has_attrname(attr_container: AttributesContainer):
    """Test has_attrname."""
    assert not attr_container.has_attrname(ATTR_NAME)


def test_has_attrname_two(attr_container_1: AttributesContainer):
    """Test has_attrname when one attribute for one key."""
    assert attr_container_1.has_attrname(ATTR_NAME)
    assert not attr_container_1.has_attrname(NO_ATTR_NAME)


# ---------------------------------------------------------------------------- #
#                                  Get Method                                  #
# ---------------------------------------------------------------------------- #
def test_get(attr_container: AttributesContainer):
    """Test get method."""
    with pytest.raises(_NoAttribute):
        attr_container.get(__KEY_0, NO_ATTR_NAME)


def test_get_one_exc(attr_container_1: AttributesContainer):
    """Test get method exc when one attr."""
    with pytest.raises(_NoKey):
        attr_container_1.get(__NO_KEY, ATTR_NAME)


def test_get_one_ok(attr_container_1: AttributesContainer):
    """Test get method exc when one attr."""
    assert attr_container_1.get(__KEY_0, ATTR_NAME) == ATTR_VAL


# ---------------------------------------------------------------------------- #
#                                Get_all Method                                #
# ---------------------------------------------------------------------------- #
def test_get_all_exc(attr_container_1: AttributesContainer):
    """Test get_all method when one attr."""
    with pytest.raises(_NoKey):
        for _, _ in attr_container_1.get_all(__NO_KEY):
            pass


def test_get_all_one(attr_container_1: AttributesContainer):
    """Test get_all method when one attr."""
    assert dict(attr_container_1.get_all(__KEY_0)) == ATTRS


# ---------------------------------------------------------------------------- #
#                                   New_attr                                   #
# ---------------------------------------------------------------------------- #
def test_new_attr(attr_container: AttributesContainer):
    """Test new_attr method."""
    attr_container.new_attr(ATTR_NAME, ATTR_DEF)
    assert (
        attr_container._attrname_index_default
        == {ATTR_NAME: (__ATTR_IND, ATTR_DEF)}
    )


def test_new_attr_one(attr_container_1: AttributesContainer):
    """Test new_attr meth when one key with one attr."""
    attr_container_1.new_attr(ATTR_NAME, ATTR_DEF)
    assert (
        attr_container_1._attrname_index_default
        == {ATTR_NAME: (__ATTR_IND, ATTR_DEF)}
    )
    assert (
        attr_container_1._lists_values
        == [[ATTR_DEF]]
    )


# ---------------------------------------------------------------------------- #
#                                    Add_key                                   #
# ---------------------------------------------------------------------------- #
def test_add_key(attr_container: AttributesContainer):
    """Test add_key meth."""
    attr_container.add_keys()
    assert attr_container._card_keys == 1
    assert not attr_container._attrname_index_default
    assert not attr_container._lists_values
    attr_container.add_keys(2)
    assert attr_container._card_keys == 3
    assert not attr_container._attrname_index_default
    assert not attr_container._lists_values


def test_add_key_one(attr_container_1: AttributesContainer):
    """Test add_key meth when there is already one key with one attr."""
    attr_container_1.add_keys()
    attr_ind, default = attr_container_1._attrname_index_default[ATTR_NAME]
    assert len(attr_container_1._lists_values[attr_ind]) == 2
    assert attr_container_1._lists_values[attr_ind][__KEY_1] == default


# ---------------------------------------------------------------------------- #
#                                   Set_attr                                   #
# ---------------------------------------------------------------------------- #
def test_set_attr_ok(attr_container_2: AttributesContainer):
    """Test set_attr method."""
    attr_container_2.set_attr(__KEY_0, ATTR_NAME, ATTR_VAL)
    assert attr_container_2._lists_values[__ATTR_IND][__KEY_0] == ATTR_VAL


def test_set_attr_exc_0(attr_container_2: AttributesContainer):
    """Test set_attr for exc _NoAttribute."""
    with pytest.raises(_NoAttribute):
        attr_container_2.set_attr(__KEY_0, NO_ATTR_NAME, ATTR_VAL)


def test_set_attr_exc_1(attr_container_2: AttributesContainer):
    """Test set_attr for exc WrongAttributeType."""
    with pytest.raises(WrongAttributeType):
        attr_container_2.set_attr(__KEY_0, ATTR_NAME, NO_ATTR_TYPE)


def test_set_attr_exc_2(attr_container_2: AttributesContainer):
    """Test set_attr for exc _NoKey."""
    with pytest.raises(_NoKey):
        attr_container_2.set_attr(__NO_KEY, ATTR_NAME, ATTR_VAL)


# ---------------------------------------------------------------------------- #
#                                  Delete_key                                  #
# ---------------------------------------------------------------------------- #
def test_delete_key_exc(attr_container_1: AttributesContainer):
    """Test delete_key method exc."""
    with pytest.raises(_NoKey):
        attr_container_1.delete_key(__NO_KEY)


def test_deleter_key_ok(attr_container_1: AttributesContainer):
    """Test delete_key method with one attr."""
    attr_container_1.delete_key(__KEY_0)
    assert attr_container_1._lists_values == [[]]
    assert attr_container_1._card_keys == 0


# ---------------------------------------------------------------------------- #
#                                  Delete_keys                                 #
# ---------------------------------------------------------------------------- #
def test_delete_keys_exc(attr_container_2: AttributesContainer):
    """Test delete_key method exc."""
    with pytest.raises(_NoKey):
        attr_container_2.delete_keys((__KEY_0, __NO_KEY))


def test_deleter_keys_ok(attr_container_2: AttributesContainer):
    """Test delete_key method with one attr."""
    attr_container_2.delete_keys((__KEY_0, __KEY_1))
    assert attr_container_2._lists_values == [[]]
    assert attr_container_2._card_keys == 0
