# -*- coding=utf-8 -*-

"""Tests for vertices container."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc


from typing import Iterator

import pytest
from bitarray import bitarray

from revsymg.exceptions import (
    NoVertexIndex,
    NoVerticesAttribute,
    WrongAttributeType,
)
from revsymg.graphs import AttributesContainer
from revsymg.graphs._adjancy_table import _AdjsT
from revsymg.graphs.split_strands_vertices import SplitStrandsVertices
from revsymg.index_lib import FORWARD_INT, IND, PRED_IND, REVERSE_INT, SUCC_IND
from tests.datatest import (
    U_F,
    U_IND,
    U_R,
    UV_IND,
    V_F,
    V_IND,
    V_R,
    VW_IND,
    W_F,
    W_IND,
    W_R,
    X_F,
    X_IND,
    X_R,
)
from tests.graphs.test_attributes import (
    ATTR_DEF,
    ATTR_NAME,
    ATTR_VAL,
    ATTRS,
    NO_ATTR_NAME,
    NO_ATTR_TYPE,
)


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
NO_INDEX = 10


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def split_strands_vertices(
        indices_adj_tab: _AdjsT) -> Iterator[SplitStrandsVertices]:
    """Yield an empty vertices container."""
    # Arrange
    _split_strands_vertices = SplitStrandsVertices(indices_adj_tab, bitarray())
    yield _split_strands_vertices
    # Clean-up


@pytest.fixture
def split_strands_vertices_attr(indices_adj_tab_u_v: _AdjsT,
                                attr_container_2: AttributesContainer) -> (
        Iterator[SplitStrandsVertices]):
    """Yield a vertices container with attribute."""
    # Arrange
    _split_strands_vertices_attr = SplitStrandsVertices(
        indices_adj_tab_u_v, bitarray(),
    )
    _split_strands_vertices_attr._attributes = attr_container_2
    _split_strands_vertices_attr._cc = bitarray('00')
    yield _split_strands_vertices_attr
    # Clean-up


@pytest.fixture
def split_strands_vertices_uf_vf(indices_adj_tab_uf_vf: _AdjsT,
                                 attr_container_2: AttributesContainer) -> (
        Iterator[SplitStrandsVertices]):
    """Yield a vertices container with edges."""
    # Arrange
    _split_strands_vertices_uf_vf = SplitStrandsVertices(
        indices_adj_tab_uf_vf, bitarray(),
    )
    _split_strands_vertices_uf_vf._attributes = attr_container_2
    _split_strands_vertices_uf_vf._cc = bitarray('00')
    yield _split_strands_vertices_uf_vf
    # Clean-up


@pytest.fixture
def split_strands_vertices_uf_vf_wr(indices_adj_tab_uf_vf_wr: _AdjsT,
                                    attr_container_3: AttributesContainer) -> (
        Iterator[SplitStrandsVertices]):
    """Yield a vertices container with edges."""
    # Arrange
    _split_strands_vertices_uf_vf_wr = SplitStrandsVertices(
        indices_adj_tab_uf_vf_wr, bitarray(),
    )
    _split_strands_vertices_uf_vf_wr._attributes = attr_container_3
    _split_strands_vertices_uf_vf_wr._cc = bitarray('001')
    yield _split_strands_vertices_uf_vf_wr
    # Clean-up


@pytest.fixture
def split_strands_vertices_uf_vf_wr_xf(indices_adj_tab_uf_vf_wr_xf: _AdjsT,
                                       attr_container_4: AttributesContainer,
                                       ) -> Iterator[SplitStrandsVertices]:
    """Yield a vertices container with all edge types."""
    # Arrange
    _split_strands_vertices_uf_vf_wr_xf = SplitStrandsVertices(
        indices_adj_tab_uf_vf_wr_xf, bitarray(),
    )
    _split_strands_vertices_uf_vf_wr_xf._attributes = attr_container_4
    _split_strands_vertices_uf_vf_wr_xf._cc = bitarray('0010')
    yield _split_strands_vertices_uf_vf_wr_xf
    # Clean-up


# ============================================================================ #
#                                TESTS FUNCTIONS                               #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                  Attr Method                                 #
# ---------------------------------------------------------------------------- #
def test_attr_no_vertex_index(
        split_strands_vertices_attr: SplitStrandsVertices):
    """Test attr method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        split_strands_vertices_attr.attr(NO_INDEX, ATTR_NAME)


def test_attr_no_vertices_attribute(
        split_strands_vertices_attr: SplitStrandsVertices):
    """Test attr method for NoVerticesAttribute exc."""
    with pytest.raises(NoVerticesAttribute):
        split_strands_vertices_attr.attr(U_IND, NO_ATTR_NAME)


def test_attr_ok(split_strands_vertices_attr: SplitStrandsVertices):
    """Test attr method with success wanted."""
    assert split_strands_vertices_attr.attr(U_IND, ATTR_NAME) == ATTR_DEF
    assert split_strands_vertices_attr.attr(V_IND, ATTR_NAME) == ATTR_VAL


# ---------------------------------------------------------------------------- #
#                                 Attrs Method                                 #
# ---------------------------------------------------------------------------- #
def test_attrs_no_vertex_index(split_strands_vertices: SplitStrandsVertices):
    """Test attrs method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in split_strands_vertices.attrs(U_IND):
            pass


def test_attrs_ok(split_strands_vertices_attr: SplitStrandsVertices):
    """Test attrs method with success wanted."""
    assert dict(split_strands_vertices_attr.attrs(V_IND)) == ATTRS


# ---------------------------------------------------------------------------- #
#                                   New_attr                                   #
# ---------------------------------------------------------------------------- #
def test_new_attr(split_strands_vertices: SplitStrandsVertices):
    """Test new_attr meth."""
    split_strands_vertices.new_attr(ATTR_NAME, ATTR_DEF)


# ---------------------------------------------------------------------------- #
#                                   Set_attr                                   #
# ---------------------------------------------------------------------------- #
def test_set_attr_ok(split_strands_vertices_attr: SplitStrandsVertices):
    """Test set_attr method."""
    split_strands_vertices_attr.set_attr(U_IND, ATTR_NAME, ATTR_VAL)


def test_set_attr_no_vertices_attr(
        split_strands_vertices_attr: SplitStrandsVertices):
    """Test set_attr method for NoverticesAttribute exc."""
    with pytest.raises(NoVerticesAttribute):
        for _ in split_strands_vertices_attr.set_attr(
                U_IND, NO_ATTR_NAME, ATTR_VAL):
            pass


def test_set_attr_wrong_type_attr(
        split_strands_vertices_attr: SplitStrandsVertices):
    """Test set_attr method for WrongAttributeType exc."""
    with pytest.raises(WrongAttributeType):
        for _ in split_strands_vertices_attr.set_attr(
                U_IND, ATTR_NAME, NO_ATTR_TYPE):
            pass


def test_set_attr_no_vertex_index(
        split_strands_vertices_attr: SplitStrandsVertices):
    """Test set_attr method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in split_strands_vertices_attr.set_attr(
                NO_INDEX, ATTR_NAME, ATTR_VAL):
            pass


# ---------------------------------------------------------------------------- #
#                                Contains_indor                                #
# ---------------------------------------------------------------------------- #
def test_contains_indor_empty(split_strands_vertices: SplitStrandsVertices):
    """Test contains_indor when empty."""
    assert not split_strands_vertices.contains_indor(U_F)


def test_contains_indor_uf_vf_wr_xf(
        split_strands_vertices_uf_vf_wr_xf: SplitStrandsVertices):
    """Test contains_indor with all edges."""
    assert split_strands_vertices_uf_vf_wr_xf.contains_indor(U_F)
    assert not split_strands_vertices_uf_vf_wr_xf.contains_indor(U_R)
    assert split_strands_vertices_uf_vf_wr_xf.contains_indor(V_F)
    assert not split_strands_vertices_uf_vf_wr_xf.contains_indor(V_R)
    assert not split_strands_vertices_uf_vf_wr_xf.contains_indor(W_F)
    assert split_strands_vertices_uf_vf_wr_xf.contains_indor(W_R)
    assert split_strands_vertices_uf_vf_wr_xf.contains_indor(X_F)
    assert not split_strands_vertices_uf_vf_wr_xf.contains_indor(X_R)


# ---------------------------------------------------------------------------- #
#                                  Orientation                                 #
# ---------------------------------------------------------------------------- #
def test_orientation_novertexindex(
        split_strands_vertices: SplitStrandsVertices):
    """Test orientation when empty."""
    with pytest.raises(NoVertexIndex):
        split_strands_vertices.orientation(U_IND)


def test_orientation_uf_vf_wr_xf(
        split_strands_vertices_uf_vf_wr_xf: SplitStrandsVertices):
    """Test orientation with all edges."""
    assert split_strands_vertices_uf_vf_wr_xf.orientation(U_IND) == FORWARD_INT
    assert split_strands_vertices_uf_vf_wr_xf.orientation(V_IND) == FORWARD_INT
    assert split_strands_vertices_uf_vf_wr_xf.orientation(W_IND) == REVERSE_INT
    assert split_strands_vertices_uf_vf_wr_xf.orientation(X_IND) == FORWARD_INT


# ---------------------------------------------------------------------------- #
#                                   Cc_record                                  #
# ---------------------------------------------------------------------------- #
def test_cc_record(split_strands_vertices_uf_vf_wr_xf: SplitStrandsVertices):
    """Test cc_record with all edges."""
    assert split_strands_vertices_uf_vf_wr_xf.cc_record() == bitarray('0010')


# ---------------------------------------------------------------------------- #
#                                  Add Method                                  #
# ---------------------------------------------------------------------------- #
def test_add_one_by_one(split_strands_vertices: SplitStrandsVertices):
    """Test add method."""
    assert split_strands_vertices.add(FORWARD_INT) == 0
    assert split_strands_vertices.add(FORWARD_INT) == 1
    assert split_strands_vertices.add(REVERSE_INT) == 2
    assert (
        len(split_strands_vertices._adjs[PRED_IND])
        == len(split_strands_vertices._adjs[SUCC_IND])
        == 3
    )
    assert split_strands_vertices._cc == bitarray('001')


# ---------------------------------------------------------------------------- #
#                                 Delete Method                                #
# ---------------------------------------------------------------------------- #
def test_delete_no_vertex_index(split_strands_vertices: SplitStrandsVertices):
    """Test delete method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        split_strands_vertices.delete(U_IND)


def test_delete_vertex_uf_vf_wr_u(
        split_strands_vertices_uf_vf_wr: SplitStrandsVertices):
    """Test delete method on u with edges."""
    split_strands_vertices_uf_vf_wr.delete(U_IND)
    assert (
        split_strands_vertices_uf_vf_wr._adjs[PRED_IND]
        == [
            [],
            [(V_IND - 1, VW_IND)],
        ]
    )
    assert (
        split_strands_vertices_uf_vf_wr._adjs[SUCC_IND]
        == [
            [
                (W_IND - 1, VW_IND),
            ],
            [],
        ]
    )
    assert split_strands_vertices_uf_vf_wr._cc == bitarray('01')


def test_delete_vertex_uf_vf_wr_v(
        split_strands_vertices_uf_vf_wr: SplitStrandsVertices):
    """Test delete method on v with edges."""
    split_strands_vertices_uf_vf_wr.delete(V_IND)
    assert (
        split_strands_vertices_uf_vf_wr._adjs[PRED_IND]
        == [
            [],
            [],
        ]
    )
    assert (
        split_strands_vertices_uf_vf_wr._adjs[SUCC_IND]
        == [
            [],
            [],
        ]
    )
    assert split_strands_vertices_uf_vf_wr._cc == bitarray('01')


def test_delete_vertex_uf_vf_wr_w(
        split_strands_vertices_uf_vf_wr: SplitStrandsVertices):
    """Test delete method on w with edges."""
    split_strands_vertices_uf_vf_wr.delete(W_IND)
    assert (
        split_strands_vertices_uf_vf_wr._adjs[PRED_IND]
        == [
            [],
            [
                (U_IND, UV_IND),
            ],
        ]
    )
    assert (
        split_strands_vertices_uf_vf_wr._adjs[SUCC_IND]
        == [
            [
                (V_IND, UV_IND),
            ],
            [],
        ]
    )
    assert split_strands_vertices_uf_vf_wr._cc == bitarray('00')


# ---------------------------------------------------------------------------- #
#                                Delete_several                                #
# ---------------------------------------------------------------------------- #
def test_delete_several_no_vertex_index(
        split_strands_vertices_uf_vf: SplitStrandsVertices):
    """Test delete method for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        split_strands_vertices_uf_vf.delete_several((U_IND, W_IND, V_IND))


def test_delete_several_uf_vf_wr_u_w(
        split_strands_vertices_uf_vf_wr: SplitStrandsVertices):
    """Test delete_several ok."""
    split_strands_vertices_uf_vf_wr.delete_several((U_IND, W_IND))
    assert (
        split_strands_vertices_uf_vf_wr._adjs[PRED_IND]
        == [
            [],
        ]
    )
    assert (
        split_strands_vertices_uf_vf_wr._adjs[SUCC_IND]
        == [
            [],
        ]
    )
    assert split_strands_vertices_uf_vf_wr._cc == bitarray('0')


# ---------------------------------------------------------------------------- #
#                                __iter__ Method                               #
# ---------------------------------------------------------------------------- #
def test_iter_empty(split_strands_vertices: SplitStrandsVertices):
    """Test iter method when empty."""
    assert not tuple(v for v in split_strands_vertices)


def test_iter_uf_vf(split_strands_vertices_uf_vf: SplitStrandsVertices):
    """Test iter method when u is deleted."""
    assert tuple(v for v in split_strands_vertices_uf_vf) == (U_IND, V_IND)


# ---------------------------------------------------------------------------- #
#                              __contains__ Method                             #
# ---------------------------------------------------------------------------- #
def test_contains_empty(split_strands_vertices: SplitStrandsVertices):
    """Test contains method when empty."""
    assert U_IND not in split_strands_vertices


def test_contains_uf_vf(split_strands_vertices_uf_vf: SplitStrandsVertices):
    """Test contains method when uf vf."""
    assert U_IND in split_strands_vertices_uf_vf
    assert V_IND in split_strands_vertices_uf_vf


# ---------------------------------------------------------------------------- #
#                                __len__ Method                                #
# ---------------------------------------------------------------------------- #
def test_len_empty(split_strands_vertices: SplitStrandsVertices):
    """Test len meth when empty."""
    assert len(split_strands_vertices) == 0


def test_len_uf_vf(split_strands_vertices_uf_vf: SplitStrandsVertices):
    """Test len meth when uf vf."""
    assert len(split_strands_vertices_uf_vf) == 2
