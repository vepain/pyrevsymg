# -*- coding=utf-8 -*-

"""Tests for edges container."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc


from typing import Iterator

import pytest

from revsymg.exceptions import (
    NoEdge,
    NoEdgeIndex,
    NoEdgesAttribute,
    NoVertexIndex,
    WrongAttributeType,
)
from revsymg.graphs import AttributesContainer
from revsymg.graphs._adjancy_table import _AdjsT
from revsymg.graphs.revsym_edges import Edges
from tests.datatest import (
    A_F,
    AV_IND,
    U_F,
    U_R,
    UA_IND,
    UV_IND,
    V_F,
    V_R,
    VW_IND,
    W_F,
    W_R,
    WX_IND,
    X_F,
    X_R,
)
from tests.graphs.test_attributes import (
    ATTR_DEF,
    ATTR_NAME,
    ATTR_VAL,
    ATTRS,
    NO_ATTR_NAME,
    NO_ATTR_TYPE,
)


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
NO_INDEX = 10


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def revsym_edges(revsym_adj_tab: _AdjsT) -> Iterator[Edges]:
    """Yield an empty edges container."""
    # Arrange
    _revsym_edges = Edges(revsym_adj_tab)
    yield _revsym_edges
    # Clean-up


@pytest.fixture
def revsym_edges_u_v(revsym_adj_tab_u_v: _AdjsT) -> (
        Iterator[Edges]):
    """Yield a edges container with two vertices and no edge."""
    # Arrange
    _revsym_edges_u_v = Edges(revsym_adj_tab_u_v)
    yield _revsym_edges_u_v
    # Clean-up


@pytest.fixture
def revsym_edges_attr(revsym_adj_tab_uf_vf: _AdjsT,
                      attr_container_1: AttributesContainer) -> (
        Iterator[Edges]):
    """Yield a edges container with one edge and one attribute."""
    # Arrange
    _revsym_edges_attr = Edges(revsym_adj_tab_uf_vf)
    _revsym_edges_attr._attributes = attr_container_1
    _revsym_edges_attr._card_e = 1
    _revsym_edges_attr._ind_e = 0
    yield _revsym_edges_attr
    # Clean-up


@pytest.fixture
def revsym_edges_uf_vf_wr(revsym_adj_tab_uf_vf_wr: _AdjsT,
                          attr_container_2: AttributesContainer) -> (
        Iterator[Edges]):
    """Yield a edges container with edges."""
    # Arrange
    _revsym_edges_uf_vf_wr = Edges(revsym_adj_tab_uf_vf_wr)
    _revsym_edges_uf_vf_wr._attributes = attr_container_2
    _revsym_edges_uf_vf_wr._card_e = 2
    _revsym_edges_uf_vf_wr._ind_e = 1
    yield _revsym_edges_uf_vf_wr
    # Clean-up


@pytest.fixture
def revsym_edges_uf_vf_wr_xf(revsym_adj_tab_uf_vf_wr_xf: _AdjsT,
                             attr_container_3: AttributesContainer) -> (
        Iterator[Edges]):
    """Yield a edges container with all edge types."""
    # Arrange
    _revsym_edges_uf_vf_wr_xf = Edges(revsym_adj_tab_uf_vf_wr_xf)
    _revsym_edges_uf_vf_wr_xf._adjs = revsym_adj_tab_uf_vf_wr_xf
    _revsym_edges_uf_vf_wr_xf._attributes = attr_container_3
    _revsym_edges_uf_vf_wr_xf._card_e = 3
    _revsym_edges_uf_vf_wr_xf._ind_e = 2
    yield _revsym_edges_uf_vf_wr_xf
    # Clean-up


@pytest.fixture
def revsym_edges_uf_af_vf(revsym_adj_tab_uf_af_vf: _AdjsT,
                          attr_container_2: AttributesContainer) -> (
        Iterator[Edges]):
    """Yield a edges container with transitive edges."""
    # Arrange
    _revsym_edges_uf_af_vf = Edges(revsym_adj_tab_uf_af_vf)
    _revsym_edges_uf_af_vf._attributes = attr_container_2
    _revsym_edges_uf_af_vf._card_e = 2
    _revsym_edges_uf_af_vf._ind_e = 1
    yield _revsym_edges_uf_af_vf
    # Clean-up


# ============================================================================ #
#                                TESTS FUNCTIONS                               #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                     Attr                                     #
# ---------------------------------------------------------------------------- #
def test_attr_no_edges_attribute(revsym_edges_attr: Edges):
    """Test attr method for NoEdgesAttribute exc."""
    with pytest.raises(NoEdgesAttribute):
        revsym_edges_attr.attr(UV_IND, NO_ATTR_NAME)


def test_attr_no_edge_index(revsym_edges_attr: Edges):
    """Test attr method for NoEdgeAttribute exc."""
    with pytest.raises(NoEdgeIndex):
        revsym_edges_attr.attr(NO_INDEX, ATTR_NAME)


def test_attr_ok(revsym_edges_attr: Edges):
    """Test attr method with success wanted."""
    assert revsym_edges_attr.attr(UV_IND, ATTR_NAME) == ATTR_VAL


# ---------------------------------------------------------------------------- #
#                                     Attrs                                    #
# ---------------------------------------------------------------------------- #
def test_attrs_no_edge_index(revsym_edges: Edges):
    """Test attrs method for NoEdgeIndex exc."""
    with pytest.raises(NoEdgeIndex):
        assert not dict(revsym_edges.attrs(UV_IND))


def test_attrs_ok(revsym_edges_attr: Edges):
    """Test attrs method with success wanted."""
    assert dict(revsym_edges_attr.attrs(UV_IND)) == ATTRS


# ---------------------------------------------------------------------------- #
#                                   New_attr                                   #
# ---------------------------------------------------------------------------- #
def test_new_attr(revsym_edges: Edges):
    """Test new_attr meth."""
    revsym_edges.new_attr(ATTR_NAME, ATTR_DEF)


# ---------------------------------------------------------------------------- #
#                                   Set_attr                                   #
# ---------------------------------------------------------------------------- #
def test_set_attr_ok(revsym_edges_attr: Edges):
    """Test set_attr method."""
    revsym_edges_attr.set_attr(UV_IND, ATTR_NAME, ATTR_VAL)


def test_set_attr_no_vertices_attr(revsym_edges_attr: Edges):
    """Test set_attr method for NoverticesAttribute exc."""
    with pytest.raises(NoEdgesAttribute):
        for _ in revsym_edges_attr.set_attr(UV_IND, NO_ATTR_NAME, ATTR_VAL):
            pass


def test_set_attr_wrong_type_attr(revsym_edges_attr: Edges):
    """Test set_attr method for WrongAttributeType exc."""
    with pytest.raises(WrongAttributeType):
        for _ in revsym_edges_attr.set_attr(UV_IND, ATTR_NAME, NO_ATTR_TYPE):
            pass


def test_set_attr_no_vertex_index(revsym_edges_attr: Edges):
    """Test set_attr method for NoVertexIndex exc."""
    with pytest.raises(NoEdgeIndex):
        for _ in revsym_edges_attr.set_attr(NO_INDEX, ATTR_NAME, ATTR_VAL):
            pass


# ---------------------------------------------------------------------------- #
#                              Biggest_edge_index                              #
# ---------------------------------------------------------------------------- #
def test_biggest_edge_index_null(revsym_edges: Edges):
    """Test biggest_edge_index meth for u NoVertexIndex esc."""
    assert revsym_edges.biggest_edge_index() == -1


def test_biggest_edge_index_ok(revsym_edges_uf_vf_wr: Edges):
    """Test biggest_edge_index meth ok."""
    assert revsym_edges_uf_vf_wr.biggest_edge_index() == 1


# ---------------------------------------------------------------------------- #
#                                Eindor_to_eind                                #
# ---------------------------------------------------------------------------- #
def test_eindor_to_eind_novertexindex_1(revsym_edges: Edges):
    """Test eindor_to_eind meth for u NoVertexIndex esc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_edges.eindor_to_eind(U_F, V_F):
            pass


def test_eindor_to_eind_novertexindex_2(revsym_edges_attr: Edges):
    """Test eindor_to_eind meth for v NoVertexIndex esc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_edges_attr.eindor_to_eind(U_F, W_R):
            pass


def test_eindor_to_eind_ok(revsym_edges_uf_vf_wr: Edges):
    """Test eindor_to_eind meth ok."""
    assert (
        tuple(revsym_edges_uf_vf_wr.eindor_to_eind(U_F, V_F))
        == (UV_IND, )
    )
    assert (
        tuple(revsym_edges_uf_vf_wr.eindor_to_eind(V_R, U_R))
        == (UV_IND, )
    )


# ---------------------------------------------------------------------------- #
#                                     Preds                                    #
# ---------------------------------------------------------------------------- #
def test_preds_novertexindex_1(revsym_edges: Edges):
    """Test preds meth for first NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_edges.preds(U_F):
            pass


def test_preds_novertexindex_2(revsym_edges: Edges):
    """Test preds meth for second NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_edges.preds(U_R):
            pass


def test_preds_vf(revsym_edges_uf_vf_wr: Edges):
    """Test preds meth for vf when two edges."""
    assert tuple(revsym_edges_uf_vf_wr.preds(V_F)) == ((U_F, UV_IND),)


def test_preds_vr(revsym_edges_uf_vf_wr: Edges):
    """Test preds meth for vr when two edges."""
    assert tuple(revsym_edges_uf_vf_wr.preds(V_R)) == ((W_F, VW_IND),)


# ---------------------------------------------------------------------------- #
#                                     Succs                                    #
# ---------------------------------------------------------------------------- #
def test_succs_novertexindex_1(revsym_edges: Edges):
    """Test succs meth for first NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_edges.succs(U_F):
            pass


def test_succs_novertexindex_2(revsym_edges: Edges):
    """Test succs meth for second NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_edges.succs(U_R):
            pass


def test_succs_vf(revsym_edges_uf_vf_wr: Edges):
    """Test succs meth for vf when two edges."""
    assert tuple(revsym_edges_uf_vf_wr.succs(V_F)) == ((W_R, VW_IND),)


def test_succs_vr(revsym_edges_uf_vf_wr: Edges):
    """Test succs meth for vr when two edges."""
    assert tuple(revsym_edges_uf_vf_wr.succs(V_R)) == ((U_R, UV_IND),)


# ---------------------------------------------------------------------------- #
#                                  Neighbours                                  #
# ---------------------------------------------------------------------------- #
def test_neighbours_novertexindex(revsym_edges: Edges):
    """Test neighbours meth for NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        for _ in revsym_edges.neighbours(U_F):
            pass


def test_neighbours_vf(revsym_edges_uf_vf_wr: Edges):
    """Test neighbours meth for vf when two edges."""
    assert (
        tuple(revsym_edges_uf_vf_wr.neighbours(V_F))
        == ((U_F, UV_IND), (W_R, VW_IND))
    )


def test_neighbours_vr(revsym_edges_uf_vf_wr: Edges):
    """Test neighbours meth for vr when two edges."""
    assert (
        tuple(revsym_edges_uf_vf_wr.neighbours(V_R))
        == ((W_F, VW_IND), (U_R, UV_IND))
    )


# ---------------------------------------------------------------------------- #
#                                      Add                                     #
# ---------------------------------------------------------------------------- #
def test_add_novertexindex_1(revsym_edges_attr: Edges):
    """Test add meth for first NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_edges_attr.add(W_F, V_R)


def test_add_novertexindex_2(revsym_edges_attr: Edges):
    """Test add meth for second NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_edges_attr.add(V_F, W_R)


def test_add_uf_vf(revsym_edges_u_v: Edges):
    """Test add meth for uf vf edge."""
    assert revsym_edges_u_v.add(U_F, V_F) == UV_IND
    assert revsym_edges_u_v._card_e == 1
    assert revsym_edges_u_v._ind_e == 0
    assert revsym_edges_u_v._adjs == (
        [
            [],
            [(U_F, UV_IND)],
        ],
        [
            [(V_F, UV_IND)],
            [],
        ],
    )
    assert revsym_edges_u_v._attributes._card_keys == 1


def test_add_vr_ur(revsym_edges_u_v: Edges):
    """Test add meth for vr ur edge."""
    assert revsym_edges_u_v.add(V_R, U_R) == UV_IND
    assert revsym_edges_u_v._card_e == 1
    assert revsym_edges_u_v._ind_e == 0
    assert revsym_edges_u_v._adjs == (
        [
            [],
            [(U_F, UV_IND)],
        ],
        [
            [(V_F, UV_IND)],
            [],
        ],
    )
    assert revsym_edges_u_v._attributes._card_keys == 1


def test_add_uf_vr(revsym_edges_u_v: Edges):
    """Test add meth for uf vr edge."""
    assert revsym_edges_u_v.add(U_F, V_R) == UV_IND
    assert revsym_edges_u_v._card_e == 1
    assert revsym_edges_u_v._ind_e == 0
    assert revsym_edges_u_v._adjs == (
        [
            [],
            [],
        ],
        [
            [(V_R, UV_IND)],
            [(U_R, UV_IND)],
        ],
    )
    assert revsym_edges_u_v._attributes._card_keys == 1


def test_add_vf_uf(revsym_edges_u_v: Edges):
    """Test add meth for vf uf edge."""
    assert revsym_edges_u_v.add(V_F, U_F) == UV_IND
    assert revsym_edges_u_v._card_e == 1
    assert revsym_edges_u_v._ind_e == 0
    assert revsym_edges_u_v._adjs == (
        [
            [(V_F, UV_IND)],
            [],
        ],
        [
            [],
            [(U_F, UV_IND)],
        ],
    )
    assert revsym_edges_u_v._attributes._card_keys == 1


# ---------------------------------------------------------------------------- #
#                                    Delete                                    #
# ---------------------------------------------------------------------------- #
def test_delete_novertexindex_1(revsym_edges_u_v: Edges):
    """Test delete meth for first NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_edges_u_v.delete(W_F, V_R, VW_IND)


def test_delete_novertexindex_2(revsym_edges_u_v: Edges):
    """Test delete meth for second NoVertexIndex exc."""
    with pytest.raises(NoVertexIndex):
        revsym_edges_u_v.delete(V_F, W_R, VW_IND)


def test_delete_noedge(revsym_edges_uf_af_vf: Edges):
    """Test delete meth for first NoEdge exc."""
    with pytest.raises(NoEdge):
        revsym_edges_uf_af_vf.delete(U_F, V_F, NO_INDEX)


def test_delete_uf_af_vf_ua(revsym_edges_uf_af_vf: Edges):
    """Test delete meth on uf af edge."""
    revsym_edges_uf_af_vf.delete(U_F, A_F, UA_IND)
    assert revsym_edges_uf_af_vf._adjs == (
        [
            [],
            [(U_F, UV_IND), (A_F, AV_IND)],
            [], [], [],
        ],
        [
            [(V_F, UV_IND)],
            [], [], [],
            [(V_F, AV_IND)],
        ],
    )


def test_delete_uf_af_vf_uv(revsym_edges_uf_af_vf: Edges):
    """Test delete meth on uf vf edge."""
    revsym_edges_uf_af_vf.delete(U_F, V_F, UV_IND)
    assert revsym_edges_uf_af_vf._adjs == (
        [
            [],
            [(A_F, AV_IND)],
            [], [],
            [(U_F, UA_IND)],
        ],
        [
            [(A_F, UA_IND)],
            [], [], [],
            [(V_F, AV_IND)],
        ],
    )


# ---------------------------------------------------------------------------- #
#                                __iter__ Method                               #
# ---------------------------------------------------------------------------- #
def test_iter_empty(revsym_edges: Edges):
    """Test iter method when empty."""
    assert not tuple(v for v in revsym_edges)


def test_iter_uf_vf_wr_xf(revsym_edges_uf_vf_wr_xf: Edges):
    """Test iter method with all edge types."""
    assert tuple(v for v in revsym_edges_uf_vf_wr_xf) == (
        (U_F, V_F, UV_IND),
        (V_R, U_R, UV_IND),
        (V_F, W_R, VW_IND),
        (W_F, V_R, VW_IND),
        (X_R, W_F, WX_IND),
        (W_R, X_F, WX_IND),
    )


# ---------------------------------------------------------------------------- #
#                              __contains__ Method                             #
# ---------------------------------------------------------------------------- #
def test_contains_empty(revsym_edges: Edges):
    """Test contains method when empty."""
    assert (U_F, V_F) not in revsym_edges


def test_contains_false_2(revsym_edges_u_v: Edges):
    """Test contains method when empty."""
    assert (V_F, W_R) not in revsym_edges_u_v


def test_contains_uf_vf_forward(revsym_edges_uf_vf_wr: Edges):
    """Test contains method when uf vf forward if."""
    assert (U_F, V_F) in revsym_edges_uf_vf_wr


def test_contains_uf_vf_reverse(revsym_edges_uf_vf_wr: Edges):
    """Test contains method when vr ur reverse if."""
    assert (V_R, U_R) in revsym_edges_uf_vf_wr


# ---------------------------------------------------------------------------- #
#                                __len__ Method                                #
# ---------------------------------------------------------------------------- #
def test_len_empty(revsym_edges: Edges):
    """Test len meth when empty."""
    assert len(revsym_edges) == 0


def test_len_uf_vf_wr(revsym_edges_uf_vf_wr: Edges):
    """Test len meth when uf vf."""
    assert len(revsym_edges_uf_vf_wr) == 4
