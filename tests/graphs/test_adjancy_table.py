# -*- coding=utf-8 -*-

"""Adjancies table test."""

# pylint: disable=redefined-outer-name, compare-to-zero,
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc

from typing import Iterator

import pytest

from revsymg.graphs._adjancy_table import (
    _AdjsIndT,
    _AdjsT,
    _build_indices_adj_table,
    _build_rev_sym_adj_table,
    _shift_indices_adj_table,
    _shift_rev_sym_adj_table,
)
from revsymg.index_lib import IND, OR, PRED_IND, SUCC_IND
from tests.datatest import (
    A_F,
    A_IND,
    AV_IND,
    U_F,
    U_IND,
    UA_IND,
    UV_IND,
    V_F,
    V_IND,
    V_R,
    VW_IND,
    W_IND,
    W_R,
    WX_IND,
    X_IND,
    X_R,
)


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                            Reverse Symmetry Tables                           #
# ---------------------------------------------------------------------------- #
@pytest.fixture
def revsym_adj_tab() -> Iterator[_AdjsT]:
    """Yield a None revsym adjancies table."""
    # Arrange
    yield _build_rev_sym_adj_table()
    # Clean-up


@pytest.fixture
def revsym_adj_tab_u_v() -> Iterator[_AdjsT]:
    """Yield a None revsym adjancies table with u v empty."""
    # Arrange
    yield (
        [[], []],
        [[], []],
    )
    # Clean-up


@pytest.fixture
def revsym_adj_tab_uf_vf() -> Iterator[_AdjsT]:
    """Yield a None revsym adjancies table with u_f -> v_f case."""
    # Arrange
    yield (
        [
            [],
            [(U_F, UV_IND)],
        ],
        [
            [(V_F, UV_IND)],
            [],
        ],
    )
    # Clean-up


@pytest.fixture
def revsym_adj_tab_uf_af_vf() -> Iterator[_AdjsT]:
    """Yield a None revsym adjancies table with u_f -> a_f -> v_f case."""
    # Arrange
    yield (
        [
            [],
            [(U_F, UV_IND), (A_F, AV_IND)],
            [], [],
            [(U_F, UA_IND)],
        ],
        [
            [(V_F, UV_IND), (A_F, UA_IND)],
            [], [], [],
            [(V_F, AV_IND)],
        ],
    )


@pytest.fixture
def revsym_adj_tab_uf_vf_wr() -> Iterator[_AdjsT]:
    """Yield a None revsym adjancies table with u_f -> v_f -> w_r case."""
    # Arrange
    yield (
        [
            [],
            [(U_F, UV_IND)],
            [],
        ],
        [
            [(V_F, UV_IND)],
            [(W_R, VW_IND)],
            [(V_R, VW_IND)],
        ],
    )
    # Clean-up


@pytest.fixture
def revsym_adj_tab_uf_vf_wr_xf() -> Iterator[_AdjsT]:
    """Yield a None revsym adjancies table with uf -> vf -> wr -> xf case."""
    # Arrange
    yield (
        [
            [],
            [(U_F, UV_IND)],
            [(X_R, WX_IND)],
            [(W_R, WX_IND)],
        ],
        [
            [(V_F, UV_IND)],
            [(W_R, VW_IND)],
            [(V_R, VW_IND)],
            [],
        ],
    )
    # Clean-up


# ---------------------------------------------------------------------------- #
#                                Indices Tables                                #
# ---------------------------------------------------------------------------- #
@pytest.fixture
def indices_adj_tab() -> Iterator[_AdjsIndT]:
    """Yield a None indices adjancies table."""
    # Arrange
    yield _build_indices_adj_table()
    # Clean-up


@pytest.fixture
def indices_adj_tab_u_v() -> Iterator[_AdjsIndT]:
    """Yield a None indices adjancies table with u v empty."""
    # Arrange
    yield (
        [[], []],
        [[], []],
    )
    # Clean-up


@pytest.fixture
def indices_adj_tab_uf_vf() -> Iterator[_AdjsIndT]:
    """Yield a None indices adjancies table with u_f -> v_f case."""
    # Arrange
    yield (
        [
            [],
            [(U_IND, UV_IND)],
        ],
        [
            [(V_IND, UV_IND)],
            [],
        ],
    )
    # Clean-up


@pytest.fixture
def indices_adj_tab_uf_af_vf() -> Iterator[_AdjsIndT]:
    """Yield a None indices adjancies table with u_f -> a_f -> v_f case."""
    # Arrange
    yield (
        [
            [],
            [(U_IND, UV_IND), (A_IND, AV_IND)],
            [], [],
            [(U_IND, UA_IND)],
        ],
        [
            [(V_IND, UV_IND), (A_IND, UA_IND)],
            [], [], [],
            [(V_IND, AV_IND)],
        ],
    )


@pytest.fixture
def indices_adj_tab_uf_vf_wr() -> Iterator[_AdjsIndT]:
    """Yield a None indices adjancies table with u_f -> v_f -> w_r case."""
    # Arrange
    yield (
        [
            [],
            [(U_IND, UV_IND)],
            [(V_IND, VW_IND)],
        ],
        [
            [(V_IND, UV_IND)],
            [(W_IND, VW_IND)],
            [],
        ],
    )
    # Clean-up


@pytest.fixture
def indices_adj_tab_uf_vf_wr_xf() -> Iterator[_AdjsIndT]:
    """Yield a None indices adjancies table with uf -> vf -> wr -> xf case."""
    # Arrange
    yield (
        [
            [],
            [(U_IND, UV_IND)],
            [(V_IND, UV_IND)],
            [(W_IND, WX_IND)],
        ],
        [
            [(V_IND, UV_IND)],
            [(W_IND, VW_IND)],
            [(X_IND, WX_IND)],
            [],
        ],
    )
    # Clean-up


# ============================================================================ #
#                                     TESTS                                    #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                 Build Rev Sym                                #
# ---------------------------------------------------------------------------- #
def test_revsym_adj_tab(revsym_adj_tab: _AdjsT):
    """Test revsym adjancies table building."""
    assert revsym_adj_tab == ([], [])


# ---------------------------------------------------------------------------- #
#                                 Shift Rev Sym                                #
# ---------------------------------------------------------------------------- #
def test_shift_revsym_u(revsym_adj_tab_uf_vf_wr: _AdjsT):
    """Test shifting revsym tab at the middle."""
    pivot = U_IND
    assert not tuple(
        _shift_rev_sym_adj_table(
            revsym_adj_tab_uf_vf_wr[PRED_IND][V_IND], pivot,
        ),
    )
    assert not tuple(
        _shift_rev_sym_adj_table(
            revsym_adj_tab_uf_vf_wr[PRED_IND][W_IND], pivot,
        ),
    )
    assert (
        tuple(
            _shift_rev_sym_adj_table(
                revsym_adj_tab_uf_vf_wr[SUCC_IND][V_IND], pivot,
            ),
        )
        == (((W_R[IND] - 1, W_R[OR]), VW_IND),)
    )
    assert (
        tuple(
            _shift_rev_sym_adj_table(
                revsym_adj_tab_uf_vf_wr[SUCC_IND][W_IND], pivot,
            ),
        )
        == (((V_R[IND] - 1, V_R[OR]), VW_IND),)
    )


def test_shift_revsym_v(revsym_adj_tab_uf_vf_wr: _AdjsT):
    """Test shifting revsym tab at the middle."""
    pivot = V_IND
    assert not tuple(
        _shift_rev_sym_adj_table(
            revsym_adj_tab_uf_vf_wr[PRED_IND][U_IND], pivot,
        ),
    )
    assert not tuple(
        _shift_rev_sym_adj_table(
            revsym_adj_tab_uf_vf_wr[PRED_IND][W_IND], pivot,
        ),
    )
    assert not tuple(
        _shift_rev_sym_adj_table(
            revsym_adj_tab_uf_vf_wr[SUCC_IND][U_IND], pivot,
        ),
    )
    assert not tuple(
        _shift_rev_sym_adj_table(
            revsym_adj_tab_uf_vf_wr[SUCC_IND][W_IND], pivot,
        ),
    )


def test_shift_revsym_w(revsym_adj_tab_uf_vf_wr: _AdjsT):
    """Test shifting revsym tab at the middle."""
    pivot = W_IND
    assert not tuple(
        _shift_rev_sym_adj_table(
            revsym_adj_tab_uf_vf_wr[PRED_IND][U_IND], pivot,
        ),
    )
    assert (
        tuple(
            _shift_rev_sym_adj_table(
                revsym_adj_tab_uf_vf_wr[PRED_IND][V_IND], pivot,
            ),
        )
        == ((U_F, UV_IND),)
    )
    assert (
        tuple(
            _shift_rev_sym_adj_table(
                revsym_adj_tab_uf_vf_wr[SUCC_IND][U_IND], pivot,
            ),
        )
        == ((V_F, UV_IND),)
    )
    assert not tuple(
        _shift_rev_sym_adj_table(
            revsym_adj_tab_uf_vf_wr[SUCC_IND][V_IND], pivot,
        ),
    )


# ---------------------------------------------------------------------------- #
#                               Build Indices Tab                              #
# ---------------------------------------------------------------------------- #
def test_indices_adj_tab(indices_adj_tab: _AdjsIndT):
    """Test indices adjancies table building."""
    assert indices_adj_tab == ([], [])


# ---------------------------------------------------------------------------- #
#                              Shift Indices Table                             #
# ---------------------------------------------------------------------------- #
def test_shift_indices_u(indices_adj_tab_uf_vf_wr: _AdjsT):
    """Test shifting indices tab at the middle."""
    pivot = U_IND
    assert not tuple(
        _shift_indices_adj_table(
            indices_adj_tab_uf_vf_wr[PRED_IND][V_IND], pivot,
        ),
    )
    assert (
        tuple(
            _shift_indices_adj_table(
                indices_adj_tab_uf_vf_wr[PRED_IND][W_IND], pivot,
            ),
        )
        == ((V_IND - 1, VW_IND),)
    )
    assert (
        tuple(
            _shift_indices_adj_table(
                indices_adj_tab_uf_vf_wr[SUCC_IND][V_IND], pivot,
            ),
        )
        == ((W_IND - 1, VW_IND),)
    )
    assert not tuple(
        _shift_indices_adj_table(
            indices_adj_tab_uf_vf_wr[SUCC_IND][W_IND], pivot,
        ),
    )


def test_shift_indices_v(indices_adj_tab_uf_vf_wr: _AdjsT):
    """Test shifting indices tab at the middle."""
    pivot = V_IND
    assert not tuple(
        _shift_indices_adj_table(
            indices_adj_tab_uf_vf_wr[PRED_IND][U_IND], pivot,
        ),
    )
    assert not tuple(
        _shift_indices_adj_table(
            indices_adj_tab_uf_vf_wr[PRED_IND][W_IND], pivot,
        ),
    )
    assert not tuple(
        _shift_indices_adj_table(
            indices_adj_tab_uf_vf_wr[SUCC_IND][U_IND], pivot,
        ),
    )
    assert not tuple(
        _shift_indices_adj_table(
            indices_adj_tab_uf_vf_wr[SUCC_IND][W_IND], pivot,
        ),
    )


def test_shift_indices_w(indices_adj_tab_uf_vf_wr: _AdjsT):
    """Test shifting indices tab at the middle."""
    pivot = W_IND
    assert not tuple(
        _shift_indices_adj_table(
            indices_adj_tab_uf_vf_wr[PRED_IND][U_IND], pivot,
        ),
    )
    assert (
        tuple(
            _shift_indices_adj_table(
                indices_adj_tab_uf_vf_wr[PRED_IND][V_IND], pivot,
            ),
        )
        == ((U_IND, UV_IND),)
    )
    assert (
        tuple(
            _shift_indices_adj_table(
                indices_adj_tab_uf_vf_wr[SUCC_IND][U_IND], pivot,
            ),
        )
        == ((V_IND, UV_IND),)
    )
    assert not tuple(
        _shift_indices_adj_table(
            indices_adj_tab_uf_vf_wr[SUCC_IND][V_IND], pivot,
        ),
    )
