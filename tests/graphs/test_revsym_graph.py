# -*- coding=utf-8 -*-

"""Tests for reverse symmetric graph."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc


from typing import Iterator

import pytest

from revsymg.exceptions import NoGraphAttribute
from revsymg.graphs.revsym_edges import Edges
from revsymg.graphs.revsym_graph import RevSymGraph
from revsymg.graphs.revsym_vertices import Vertices


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
__ATTR_NAME_1 = 'attr_name_1'
__ATTR_VALUE_1 = 'attr_value'

__ATTR_NAME_2 = 'attr_name_2'
__ATTR_VALUE_2 = 42


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def revsym_graph() -> Iterator[RevSymGraph]:
    """Yield an empty graph."""
    # Arrange
    revsym_graph = RevSymGraph()
    yield revsym_graph
    # Clean-up


@pytest.fixture
def revsym_graph_attr() -> Iterator[RevSymGraph]:
    """Yield an empty graph."""
    # Arrange
    revsym_graph = RevSymGraph()
    revsym_graph._attributes = {
        __ATTR_NAME_1: __ATTR_VALUE_1,
        __ATTR_NAME_2: __ATTR_VALUE_2,
    }
    yield revsym_graph
    # Clean-up


@pytest.fixture
def revsym_graph_all(revsym_vertices_uf_vf_wr_xf: Vertices,
                     revsym_edges_uf_vf_wr_xf: Edges) -> Iterator[RevSymGraph]:
    """Yield a graph with all edge types."""
    # Arrange
    revsym_graph = RevSymGraph()
    revsym_graph._vertices = revsym_vertices_uf_vf_wr_xf
    revsym_graph._edges = revsym_edges_uf_vf_wr_xf
    revsym_graph._attributes = {
        __ATTR_NAME_1: __ATTR_VALUE_1,
        __ATTR_NAME_2: __ATTR_VALUE_2,
    }
    yield revsym_graph
    # Clean-up


# ============================================================================ #
#                                TEST FUNCTIONS                                #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                     Attr                                     #
# ---------------------------------------------------------------------------- #
def test_attr_empty(revsym_graph: RevSymGraph):
    """Test attr meth when empty."""
    with pytest.raises(NoGraphAttribute):
        revsym_graph.attr(__ATTR_NAME_1)


def test_attr_ok(revsym_graph_attr: RevSymGraph):
    """Test attr meth with attrs."""
    assert revsym_graph_attr.attr(__ATTR_NAME_1) == __ATTR_VALUE_1
    assert revsym_graph_attr.attr(__ATTR_NAME_2) == __ATTR_VALUE_2


# ---------------------------------------------------------------------------- #
#                                     Attrs                                    #
# ---------------------------------------------------------------------------- #
def test_attrs_empty(revsym_graph: RevSymGraph):
    """Test attrs meth when empty."""
    assert not dict(revsym_graph.attrs())


def test_attrs_ok(revsym_graph_attr: RevSymGraph):
    """Test attrs meth with attrs."""
    assert revsym_graph_attr.attrs() == {
        __ATTR_NAME_1: __ATTR_VALUE_1,
        __ATTR_NAME_2: __ATTR_VALUE_2,
    }


# ---------------------------------------------------------------------------- #
#                                   Set_attr                                   #
# ---------------------------------------------------------------------------- #
def test_set_attr(revsym_graph: RevSymGraph):
    """Test set_attr meth when empty."""
    revsym_graph.set_attr(__ATTR_NAME_1, __ATTR_VALUE_1)
    assert revsym_graph._attributes == {
        __ATTR_NAME_1: __ATTR_VALUE_1,
    }
    revsym_graph.set_attr(__ATTR_NAME_1, __ATTR_VALUE_2)
    assert revsym_graph._attributes == {
        __ATTR_NAME_1: __ATTR_VALUE_2,
    }


# ---------------------------------------------------------------------------- #
#                                    Is_attr                                   #
# ---------------------------------------------------------------------------- #
def test_is_attr_empty(revsym_graph: RevSymGraph):
    """Test ais_attr meth when empty."""
    assert not revsym_graph.is_attr(__ATTR_NAME_1)


def test_is_attr_ok(revsym_graph_attr: RevSymGraph):
    """Test is_attr meth with attrs."""
    assert revsym_graph_attr.is_attr(__ATTR_NAME_1)
    assert revsym_graph_attr.is_attr(__ATTR_NAME_2)


# ---------------------------------------------------------------------------- #
#                                   Vertices                                   #
# ---------------------------------------------------------------------------- #
def test_vertices(revsym_graph: RevSymGraph):
    """Test vertices getter."""
    assert isinstance(revsym_graph.vertices(), Vertices)


# ---------------------------------------------------------------------------- #
#                                     Edges                                    #
# ---------------------------------------------------------------------------- #
def test_edges(revsym_graph: RevSymGraph):
    """Test edges getter."""
    assert isinstance(revsym_graph.edges(), Edges)
