# -*- coding=utf-8 -*-

"""Tests for reverse symmetric graph."""

# pylint: disable=redefined-outer-name, compare-to-zero, protected-access
# pylint: disable=missing-param-doc, missing-yield-doc, missing-raises-doc


from typing import Iterator

import pytest

from revsymg.exceptions import NoGraphAttribute
from revsymg.graphs.revsym_edges import Edges
from revsymg.graphs.revsym_vertices import Vertices
from revsymg.graphs.split_strands_edges import SplitStrandsEdges
from revsymg.graphs.split_strands_graph import SplitStrandsGraph
from revsymg.graphs.split_strands_vertices import SplitStrandsVertices


# ============================================================================ #
#                                   CONSTANTS                                  #
# ============================================================================ #
__ATTR_NAME_1 = 'attr_name_1'
__ATTR_VALUE_1 = 'attr_value'

__ATTR_NAME_2 = 'attr_name_2'
__ATTR_VALUE_2 = 42


# ============================================================================ #
#                                    FIXTURE                                   #
# ============================================================================ #
@pytest.fixture
def split_strands_graph() -> Iterator[SplitStrandsGraph]:
    """Yield an empty graph."""
    # Arrange
    split_strands_graph = SplitStrandsGraph()
    yield split_strands_graph
    # Clean-up


@pytest.fixture
def split_strands_graph_attr() -> Iterator[SplitStrandsGraph]:
    """Yield an empty graph."""
    # Arrange
    split_strands_graph = SplitStrandsGraph()
    split_strands_graph._attributes = {
        __ATTR_NAME_1: __ATTR_VALUE_1,
        __ATTR_NAME_2: __ATTR_VALUE_2,
    }
    yield split_strands_graph
    # Clean-up


@pytest.fixture
def split_strands_graph_all(
        revsym_vertices_uf_vf_wr_xf: Vertices,
        revsym_edges_uf_vf_wr_xf: Edges) -> Iterator[SplitStrandsGraph]:
    """Yield a graph with all edge types."""
    # Arrange
    split_strands_graph = SplitStrandsGraph()
    split_strands_graph._vertices = revsym_vertices_uf_vf_wr_xf
    split_strands_graph._edges = revsym_edges_uf_vf_wr_xf
    split_strands_graph._attributes = {
        __ATTR_NAME_1: __ATTR_VALUE_1,
        __ATTR_NAME_2: __ATTR_VALUE_2,
    }
    yield split_strands_graph
    # Clean-up


# ============================================================================ #
#                                TEST FUNCTIONS                                #
# ============================================================================ #
# ---------------------------------------------------------------------------- #
#                                     Attr                                     #
# ---------------------------------------------------------------------------- #
def test_attr_empty(split_strands_graph: SplitStrandsGraph):
    """Test attr meth when empty."""
    with pytest.raises(NoGraphAttribute):
        split_strands_graph.attr(__ATTR_NAME_1)


def test_attr_ok(split_strands_graph_attr: SplitStrandsGraph):
    """Test attr meth with attrs."""
    assert split_strands_graph_attr.attr(__ATTR_NAME_1) == __ATTR_VALUE_1
    assert split_strands_graph_attr.attr(__ATTR_NAME_2) == __ATTR_VALUE_2


# ---------------------------------------------------------------------------- #
#                                     Attrs                                    #
# ---------------------------------------------------------------------------- #
def test_attrs_empty(split_strands_graph: SplitStrandsGraph):
    """Test attrs meth when empty."""
    assert not dict(split_strands_graph.attrs())


def test_attrs_ok(split_strands_graph_attr: SplitStrandsGraph):
    """Test attrs meth with attrs."""
    assert split_strands_graph_attr.attrs() == {
        __ATTR_NAME_1: __ATTR_VALUE_1,
        __ATTR_NAME_2: __ATTR_VALUE_2,
    }


# ---------------------------------------------------------------------------- #
#                                   Set_attr                                   #
# ---------------------------------------------------------------------------- #
def test_set_attr(split_strands_graph: SplitStrandsGraph):
    """Test set_attr meth when empty."""
    split_strands_graph.set_attr(__ATTR_NAME_1, __ATTR_VALUE_1)
    assert split_strands_graph._attributes == {
        __ATTR_NAME_1: __ATTR_VALUE_1,
    }
    split_strands_graph.set_attr(__ATTR_NAME_1, __ATTR_VALUE_2)
    assert split_strands_graph._attributes == {
        __ATTR_NAME_1: __ATTR_VALUE_2,
    }


# ---------------------------------------------------------------------------- #
#                                   Vertices                                   #
# ---------------------------------------------------------------------------- #
def test_vertices(split_strands_graph: SplitStrandsGraph):
    """Test vertices getter."""
    assert isinstance(split_strands_graph.vertices(), SplitStrandsVertices)


# ---------------------------------------------------------------------------- #
#                                     Edges                                    #
# ---------------------------------------------------------------------------- #
def test_edges(split_strands_graph: SplitStrandsGraph):
    """Test edges getter."""
    assert isinstance(split_strands_graph.edges(), SplitStrandsEdges)
